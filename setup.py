import sys
from setuptools import Command
from setuptools import setup
from setuptools.extension import Extension
from distutils.command.build_ext import build_ext as DistUtilsBuildExt


class BuildExtension(Command):
    description = DistUtilsBuildExt.description
    user_options = DistUtilsBuildExt.user_options
    boolean_options = DistUtilsBuildExt.boolean_options
    help_options = DistUtilsBuildExt.help_options

    def __init__(self, *args, **kwargs):
        from setuptools.command.build_ext import build_ext as SetupToolsBuildExt

        # Bypass __setatrr__ to avoid infinite recursion.
        self.__dict__['_command'] = SetupToolsBuildExt(*args, **kwargs)

    def __getattr__(self, name):
        return getattr(self._command, name)

    def __setattr__(self, name, value):
        setattr(self._command, name, value)

    def initialize_options(self, *args, **kwargs):
        return self._command.initialize_options(*args, **kwargs)

    def finalize_options(self, *args, **kwargs):
        ret = self._command.finalize_options(*args, **kwargs)
        import numpy
        self.include_dirs.append(numpy.get_include())
        return ret

    def run(self, *args, **kwargs):
        return self._command.run(*args, **kwargs)


setup(
    name='anunnaki',
    version='3.0.0',
    description='Anunnaki - SBSE Framework for Exploring Uncertain System Behavior',
    url='http://localhost',
    author='Michael Austin Langford',
    author_email='langfo37@msu.edu',
    classifiers=[
        'Programming Language :: Python',
        'License :: Freely Distributable',
        'Operating System :: Microsoft :: Windows',
        'Development Status :: 1 - Planning',
        'Intended Audience :: Education',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    packages=[
        'adapa',
        'adapa.backend',
        'adapa.backend.ev3',
        'adapa.backend.jetbot',
        'adapa.rospy',
        'adapa.rospy.control',
        'adapa.rospy.dnn',
        'adapa.rospy.ev3',
        'adapa.rospy.gazebo',
        'adapa.rospy.jetbot',
        'adapa.rospy.user',
        'adapa.rospy.util',
    ],
    include_package_data=True,
    package_data={},
    setup_requires=[],
    install_requires=[],
    scripts=[],
    cmdclass={'build_ext': BuildExtension},
    ext_modules=[],
)

# Adapa Framework

Controls a combination of NVIDIA Jetson (Jetbot) and Lego Mindstorm EV3 robotic components.

# dev notes
clone repo
create softlink to adapa/adapa_ros in catkin_ws/src
create softlink to adapa/adapa in catkin_ws/src/adapa_ros/src

# ev3 notes (install on PC)
sudo apt-get install paho-mqtt

# ev3 notes (on ev3)
create mqtt-manager.service in /etc/systemd/sytem
sudo systemctl enable mqtt-manager.service
sudo systemctl start mqtt-manager.service


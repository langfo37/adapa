import numpy as np


class MovingAverageFilter(object):
    def __init__(self, window=10, ignore_outliers=True):
        self._window = window
        self._ignore_outliers = ignore_outliers
        self._history = list()
        self._mean = np.nan
        self._std = np.nan

    def __call__(self, value):
        self._history.append(value)
        if len(self._history) > self._window:
            self._history.pop(0)
        values = np.array(self._history)
        if self._ignore_outliers and len(values) > 3 and np.isnan(self._mean) and not np.isnan(self._std):
            values = values[np.abs(self._mean - values) < 3 * self._std]
            self._mean = np.mean(values)
            self._std = np.std(values)
        elif len(values) > 0:
            self._mean = np.mean(values)
            self._std = np.std(values)
        return self._mean

from PIL import Image
import cv2
import numpy as np
import threading


class CameraController(object):
    def __init__(self, target_size=(160, 120)):
        self._capture_image = Image.new(mode='RGB', size=target_size)
        self._capture_thread = threading.Thread(target=self._capture_loop)

        try:
            self._capture = cv2.VideoCapture(0)
            retval, image = self._capture.read()
            if retval:
                self._capture_image = self._post_process_image(image)
            else:
                raise RuntimeError('Failed to read from video capture stream.')
            self.start()
        except:
            self.stop()
            raise RuntimeError('Failed to initialize video capture stream.')

    @property
    def capture_image(self):
        return self._capture_image

    def start(self):
        if not self._capture.isOpened():
            self._capture.open(0)
        if self._capture_thread is not None and not self._capture_thread.isAlive():
            self._capture_thread.start()

    def stop(self):
        self._capture.release()
        if self._capture_thread and self._capture_thread.is_alive():
            self._capture_thread.join()

    def _capture_loop(self):
        while True:
            retval, image = self._capture.read()
            if retval:
                self._capture_image = self._post_process_image(image)
            else:
                break

    def _post_process_image(self, image):
        image = Image.fromarray(np.flip(image, axis=2), mode='RGB').resize(self._capture_image.size)
        return image

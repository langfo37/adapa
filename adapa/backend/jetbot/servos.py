import RPi.GPIO as GPIO
import serial
import time

# set the GPIO port to BCM encoding mode.
GPIO.setmode(GPIO.BCM)


class CameraServoController(object):
    def __init__(self):
        self._serial = serial.Serial("/dev/ttyTHS1", 115200, timeout=0.001)

        # settings for left/right movement
        self._leftright_center = 2048 + 45
        self._leftright_delta = 15
        self._leftright_min = self._leftright_center - self._leftright_delta * 45
        self._leftright_max = self._leftright_center + self._leftright_delta * 45
        self._leftright_value = self._leftright_center

        # settings for up/down movement
        self._updown_center = 2048
        self._updown_delta = 15
        self._updown_min = self._updown_center - self._updown_delta * 15
        self._updown_max = self._updown_center + self._updown_delta * 45
        self._updown_value = self._updown_center

        # reset camera servos
        self.cam_reset()

    def cam_down(self):
        self._updown_value = max(self._updown_min, self._updown_value - self._updown_delta)
        self._servo_update(2, self._updown_value)

    def cam_left(self):
        self._leftright_value = min(self._leftright_max, self._leftright_value + self._leftright_delta)
        self._servo_update(1, self._leftright_value)

    def cam_reset(self):
        self._leftright_value = self._leftright_center
        self._updown_value = self._updown_center
        self._servo_update(1, self._leftright_value)
        time.sleep(0.1)
        self._servo_update(2, self._updown_value)

    def cam_right(self):
        self._leftright_value = max(self._leftright_min, self._leftright_value - self._leftright_delta)
        self._servo_update(1, self._leftright_value)

    def cam_up(self):
        self._updown_value = min(self._updown_max, self._updown_value + self._updown_delta)
        self._servo_update(2, self._updown_value)

    def stop(self):
        if self._serial:
            self._serial.close()

    def _servo_update(self, index, angle):
        if index == 1:
            angle = max(self._leftright_min, min(self._leftright_max, angle))
        elif index == 2:
            angle = max(self._updown_min, min(self._updown_max, angle))

        pack1 = 0xff
        pack2 = 0xff
        id = index
        len = 0x07
        cmd = 0x03
        addr = 0x2A
        pos_H = (angle >> 8) & 0x00ff
        pos_L = angle & 0x00ff
        time_H = 0x00
        time_L = 0x0A
        checknum = (~(id + len + cmd + addr + pos_H + pos_L + time_H + time_L)) & 0xff
        data = [pack1, pack2, id, len, cmd, addr, pos_H, pos_L, time_H, time_L, checknum]
        self._serial.write(bytes(data))

from Adafruit_MotorHAT import Adafruit_MotorHAT


class MotorController(object):
    def __init__(
            self,
            i2c_bus=1,
            left_motor_channel=1,
            left_motor_alpha=1.0,
            right_motor_channel=2,
            right_motor_alpha=1.0,
            vertical_motor_channel=3,
            vertical_motor_alpha=1.0,
            bln_motor_channel=4,
            bln_motor_alpha=1.0,
    ):
        self._i2c_bus = i2c_bus
        self._motor_driver = Adafruit_MotorHAT(i2c_bus=i2c_bus)
        self._left_motor = Motor(self._motor_driver, channel=left_motor_channel, alpha=left_motor_alpha)
        self._right_motor = Motor(self._motor_driver, channel=right_motor_channel, alpha=right_motor_alpha)
        self._vertical_motor = Motor(self._motor_driver, channel=vertical_motor_channel, alpha=vertical_motor_alpha)
        self._bln_motor = Motor(self._motor_driver, channel=bln_motor_channel, alpha=bln_motor_alpha)

    def drive_backward(self, speed=1.0):
        self._left_motor.speed = -speed
        self._right_motor.speed = -speed

    def drive_forward(self, speed=1.0):
        self._left_motor.speed = speed
        self._right_motor.speed = speed

    def drive_left(self, speed=1.0):
        self._left_motor.speed = -speed
        self._right_motor.speed = speed

    def drive_right(self, speed=1.0):
        self._left_motor.speed = speed
        self._right_motor.speed = -speed

    def drive_stop(self):
        self._left_motor.speed = 0
        self._right_motor.speed = -0

    def rise_down(self, speed=1.0):
        self._vertical_motor.speed = speed

    def rise_stop(self):
        self._vertical_motor.speed = 0

    def rise_up(self, speed=1.0):
        self._vertical_motor.speed = -speed

    def set_drive_motors(self, left_speed, right_speed):
        self._left_motor.speed = left_speed
        self._right_motor.speed = right_speed

    def set_bln(self, bln_brightness):
        self._bln_motor.speed = bln_brightness

    def stop(self):
        self.drive_stop()
        self.rise_stop()


class Motor(object):
    def __init__(self, driver, channel, alpha=1.0, beta=0.0):
        self._driver = driver
        self._motor = self._driver.getMotor(channel)
        self._alpha = alpha
        self._beta = beta
        self._speed = 0

    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, value):
        value = int(255.0 * (self._alpha * value + self._beta))
        self._speed = min(max(abs(value), 0), 255)
        if value < 0:
            self._motor.run(Adafruit_MotorHAT.FORWARD, self._speed)
        else:
            self._motor.run(Adafruit_MotorHAT.BACKWARD, self._speed)

    def _release(self):
        self._motor.run(Adafruit_MotorHAT.RELEASE, 0)

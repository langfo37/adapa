import Adafruit_GPIO.I2C as I2C


class BatteryController(object):
    def __init__(self):
        self._device = I2C.get_i2c_device(0x1b, busnum=1)

    @staticmethod
    def categorize_status(voltage_level):
        if voltage_level >= 11.5:
            return 'high'
        elif voltage_level >= 11.0:
            return 'medium'
        else:
            return 'low'

    def check_voltage(self):
        ad_value = self._device.readList(0x00, 2)
        voltage_level = ((ad_value[0] << 8) + ad_value[1]) * 13.3 / 1023.0
        return voltage_level

    def stop(self):
        pass

import RPi.GPIO as GPIO
import time

# set the GPIO port to BCM encoding mode.
GPIO.setmode(GPIO.BCM)


class BuzzerController(object):
    def __init__(self):
        self._pin = 6

        # set board pin-numbering scheme
        GPIO.setmode(GPIO.BCM)

        # set pin as an output pin
        GPIO.setup(self._pin, GPIO.OUT, initial=GPIO.LOW)

    def buzz_on(self):
        GPIO.output(self._pin, GPIO.HIGH)

    def buzz_off(self):
        GPIO.output(self._pin, GPIO.LOW)

    def buzz_timed(self, duration=0.25):
        GPIO.output(self._pin, GPIO.HIGH)
        time.sleep(duration)
        GPIO.output(self._pin, GPIO.LOW)

    def stop(self):
        GPIO.cleanup()

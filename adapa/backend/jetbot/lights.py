import Adafruit_GPIO.I2C as I2C
import time

class LightController(object):
    def __init__(self):
        self._device = I2C.get_i2c_device(0x1b, busnum=1)

    def reset(self):
        self.set_rgb_all(0x01, 0x01, 0x01)

    def set_effect_breath(self):
        self._device.write8(0x04, 0x01)

    def set_effect_chameleon(self):
        self._device.write8(0x04, 0x02)

    def set_effect_waterfall(self):
        self._device.write8(0x04, 0x00)

    def set_rgb_all(self, r, g, b):
        self._device.write8(0x00, 0xFF)
        self._device.write8(0x01, r)
        self._device.write8(0x02, g)
        self._device.write8(0x03, b)

    def set_rgb_pos(self, r, g, b, pos):
        if pos <= 0x09:
            self._device.write8(0x00, pos)
            self._device.write8(0x01, r)
            self._device.write8(0x02, g)
            self._device.write8(0x03, b)

    def stop(self):
        self.reset()
        time.sleep(0.1)
        self.reset()

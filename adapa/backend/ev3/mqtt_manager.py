#!/usr/bin/env python3

from datetime import datetime

from ev3dev2 import DeviceNotFound
from ev3dev2.button import Button
from ev3dev2.led import Leds
from ev3dev2.motor import LargeMotor
from ev3dev2.motor import MediumMotor
from ev3dev2.motor import Motor
from ev3dev2.sensor import list_sensors
from ev3dev2.sensor.lego import ColorSensor
from ev3dev2.sensor.lego import GyroSensor
from ev3dev2.sensor.lego import TouchSensor
from ev3dev2.sensor.lego import UltrasonicSensor
from ev3dev2.power import PowerSupply
from ev3dev2.sound import Sound
from queue import Queue
import argparse
import json
import paho.mqtt.client as mqtt
import time
import threading

# define color labels
COLOR_LABELS = ['none', 'black', 'blue', 'green', 'yellow', 'red', 'white', 'brown']


class MQTTManager(object):
    def __init__(self, mqtt_ip='192.168.0.1', mqtt_keepalive=60, mqtt_port=1883, verbose=False):
        self._mqtt_ip = mqtt_ip
        self._mqtt_keepalive = mqtt_keepalive
        self._mqtt_port = mqtt_port
        self._verbose = verbose

        # define component topics and thread types
        self._components = [
            dict(topic='ev3/power_sensor', thread=PowerSensorThread, kwargs=dict(refresh_rate=2)),
            dict(topic='ev3/ultrasonic_sensor', thread=UltrasonicSensorThread, kwargs=dict(refresh_rate=15)),
            dict(topic='ev3/touch_sensor', thread=TouchSensorThread, kwargs=dict(refresh_rate=15)),
            dict(topic='ev3/arm_actuator', thread=ArmActuatorThread, kwargs=dict(refresh_rate=15)),
            dict(topic='ev3/hand_actuator', thread=HandActuatorThread, kwargs=dict(refresh_rate=15)),
            # dict(topic='ev3/gyro_sensor', thread=GyroSensorThread, kwargs=dict(refresh_rate=15)),
            # dict(topic='ev3/color_sensor', thread=ColorSensorThread, kwargs=dict()),
            # dict(topic='ev3/leds_actuator', thread=LEDsActuatorThread, kwargs=dict()),
            # dict(topic='ev3/speech_actuator', thread=SpeechActuatorThread, kwargs=dict()),
        ]

        # init clients and threads for each component
        for component in self._components:
            if isinstance(component, dict):
                component['client'] = self._init_client(component['topic'])
                component['thread'] = component['thread'](component['client'], component['topic'], **component['kwargs'])

    def run(self):
        # notify user
        if self._verbose:
            print('[{}] initialized.'.format(self.__class__.__name__))

        # speak
        sound = Sound()
        sound.speak('E V 3 enabled', play_type=Sound.PLAY_WAIT_FOR_COMPLETE)

        # start all threads
        for component in self._components:
            thread = component.get('thread')
            if isinstance(thread, MQTTClientThread):
                thread.start()

        # loop until a button is pressed
        button = Button()
        while not button.any():
            time.sleep(0.1)

    def terminate(self):
        # terminate all threads and clients
        for component in self._components:
            # terminate thread
            thread = component.get('thread')
            if isinstance(thread, MQTTClientThread) and thread.is_alive():
                thread.stop()
                thread.join()

            # terminate client
            client = component.get('client')
            if isinstance(client, mqtt.Client):
                client.loop_stop()

        # notify user
        if self._verbose:
            print('[{}] terminated.'.format(self.__class__.__name__))

        # speak
        sound = Sound()
        sound.speak('E V 3 disabled', play_type=Sound.PLAY_WAIT_FOR_COMPLETE)

    def _init_client(self, topic):
        client_name = topic.split('/')[-1]
        client_timestamp = datetime.now().strftime('%Y%m%d_%H%M%S%f')
        client_id = '{}_{}'.format(client_name, client_timestamp)

        # connect client
        client = mqtt.Client(client_id, protocol=mqtt.MQTTv311)
        client.connect(self._mqtt_ip, port=self._mqtt_port, keepalive=self._mqtt_keepalive)

        # start client loop in another thread
        client.loop_start()
        return client


class MQTTClientThread(threading.Thread):
    def __init__(self, client, topic, *args, verbose=True, **kwargs):
        super().__init__(*args, **kwargs)
        self._client = client
        self._topic = topic
        self._stopped = threading.Event()
        self._verbose = verbose

    def stop(self):
        self._stopped.set()

    def stopped(self):
        return self._stopped.is_set()


class MQTTActuatorThread(MQTTClientThread):
    def __init__(self, *args, refresh_rate=15, **kwargs):
        super().__init__(*args, **kwargs)
        self._request_queue = Queue()
        self._request_topic = '{}/request'.format(self._topic)
        self._response_topic = '{}/response'.format(self._topic)
        self._refresh_rate = refresh_rate

    def run(self):
        if self._verbose:
            print('[{}] initialized.'.format(self.__class__.__name__))

        # initialize device
        self._init_device()

        # subscribe to topic
        if self._client is not None:
            self._client.subscribe(self._request_topic)
            self._client.on_message = self._on_request_cb

        # continuously process requests
        while not self._stopped.is_set():
            start = time.time()

            # get latest request
            request_payload = self._fetch_request_payload()
            if request_payload is not None:
                # process request payload
                response_payload = self._process_request(request_payload)
                if response_payload is not None:
                    # publish response
                    response_payload['_uuid'] = request_payload.get('_uuid')
                    self._client.publish(self._response_topic, json.dumps(response_payload))

            # sleep until next iteration
            elapsed = time.time() - start
            delay = max(0.0, (1.0 / self._refresh_rate) - elapsed)
            time.sleep(delay)

        # terminate device
        self._term_device()

        if self._verbose:
            print('[{}] terminated.'.format(self.__class__.__name__))

    def _fetch_request_payload(self):
        # ignore all but most recent request
        request_payload = None
        while self._request_queue.qsize() > 0:
            request_payload = self._request_queue.get(block=False)
        return request_payload

    def _init_device(self):
        raise NotImplementedError()

    def _on_request_cb(self, client, userdata, msg):
        # queue request payload
        if msg.topic == self._request_topic:
            request_payload = json.loads(str(msg.payload.decode('utf-8', 'ignore')))
            self._request_queue.put(request_payload)

    def _process_request(self, payload):
        raise NotImplementedError()

    def _term_device(self):
        raise NotImplementedError()


class MQTTSensorThread(MQTTClientThread):
    def __init__(self, *args, refresh_rate=15, **kwargs):
        super().__init__(*args, **kwargs)
        self._refresh_rate = refresh_rate

    def run(self):
        if self._verbose:
            print('[{}] initialized.'.format(self.__class__.__name__))

        # initialize device
        self._init_device()

        # monitor and publish sensor values
        while not self._stopped.is_set():
            start = time.time()

            try:
                # publish mqtt payload
                payload = self._create_message_payload()
                if payload is not None:
                    self._client.publish(self._topic, json.dumps(payload))
            except OSError:
                pass

            # sleep until next iteration
            elapsed = time.time() - start
            delay = max(0.0, (1.0 / self._refresh_rate) - elapsed)
            time.sleep(delay)

        # terminate device
        self._term_device()

        if self._verbose:
            print('[{}] terminated.'.format(self.__class__.__name__))

    def _create_message_payload(self):
        raise NotImplementedError()

    def _init_device(self):
        raise NotImplementedError()

    def _term_device(self):
        raise NotImplementedError()


class ArmActuatorThread(MQTTActuatorThread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._device = None
        self._angle_curr = 0.0
        self._angle_min = 0.0
        self._angle_max = 90.0
        self._speed = 200

    def _init_device(self):
        try:
            self._device = MediumMotor()
            self._device.position = 0
        except DeviceNotFound:
            self._device = None

    def _process_request(self, req_payload):
        # check if device is connected and reinitialize if not
        if self._device is None:
            self._init_device()
            resp_payload = dict(error='Device disconnected.')
            return resp_payload

        # check action
        action = req_payload.get('action')
        if not isinstance(action, str):
            resp_payload = dict(error='Invalid action.')
            return resp_payload

        # determine angle delta
        angle_delta = 0
        if action.lower() == 'up' or action.lower() == 'open':
            angle_delta = self._angle_max - self._angle_curr
        elif action.lower() == 'down' or action.lower() == 'close':
            angle_delta = self._angle_min - self._angle_curr

        # run motor
        resp_payload = dict(error=None)
        try:
            if abs(angle_delta) > 1e-6 and self._speed > 1e-6:
                self._angle_curr += angle_delta
                self._device.run_to_rel_pos(position_sp=angle_delta, speed_sp=self._speed, stop_action=Motor.STOP_ACTION_BRAKE)
                self._device.wait_while(Motor.STATE_RUNNING)
        except DeviceNotFound:
            self._init_device()
            resp_payload = dict(error='Device disconnected.')
        return resp_payload

    def _term_device(self):
        if self._device is not None:
            self._device.stop()


class ColorSensorThread(MQTTSensorThread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._device = None

    def _init_device(self):
        try:
            self._device = ColorSensor()
        except DeviceNotFound:
            self._device = None

    def _create_message_payload(self):
        # check if device is connected and reinitialize if not
        if self._device is None:
            self._init_device()
            return None

        # create payload
        payload = None
        try:
            payload = dict()
            self._device.mode = 'COL-AMBIENT'
            payload['ambient'] = self._device.value()
            self._device.mode = 'COL-REFLECT'
            payload['reflected'] = self._device.value()
            self._device.mode = 'COL-COLOR'
            payload['color'] = COLOR_LABELS[self._device.value()]
        except DeviceNotFound:
            # reinitialize device if not found
            self._init_device()
        return payload

    def _term_device(self):
        pass


class GyroSensorThread(MQTTSensorThread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._device = None

    def _init_device(self):
        try:
            self._device = GyroSensor()
        except DeviceNotFound:
            self._device = None

    def _create_message_payload(self):
        # check if device is connected and reinitialize if not
        if self._device is None:
            self._init_device()
            return None

        # create payload
        payload = None
        try:
            payload = dict()
            self._device.mode = 'GYRO-ANG'
            payload['angle'] = self._device.value()
            self._device.reset()
        except DeviceNotFound:
            # reinitialize device if not found
            self._init_device()
        return payload

    def _term_device(self):
        pass


class HandActuatorThread(MQTTActuatorThread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._device = None
        self._angle_curr = 0.0
        self._angle_min = 0.0
        self._angle_max = 30.0
        self._speed = 200

    def _init_device(self):
        try:
            self._device = LargeMotor()
            self._device.position = 0
        except DeviceNotFound:
            self._device = None

    def _process_request(self, req_payload):
        # check if device is connected and reinitialize if not
        if self._device is None:
            self._init_device()
            resp_payload = dict(error='Device disconnected.')
            return resp_payload

        action = req_payload.get('action')
        if not isinstance(action, str):
            resp_payload = dict(error='Invalid action.')
            return resp_payload

        # determine angle delta
        angle_delta = 0
        if action.lower() == 'up' or action.lower() == 'open':
            angle_delta = self._angle_max - self._angle_curr
        elif action.lower() == 'down' or action.lower() == 'close':
            angle_delta = self._angle_min - self._angle_curr

        # run motor
        resp_payload = dict(error=None)
        try:
            if abs(angle_delta) > 1e-6 and self._speed > 1e-6:
                self._angle_curr += angle_delta
                self._device.run_to_rel_pos(position_sp=angle_delta, speed_sp=self._speed, stop_action=Motor.STOP_ACTION_BRAKE)
                self._device.wait_while(Motor.STATE_RUNNING)
        except DeviceNotFound:
            self._init_device()
            resp_payload = dict(error='Device disconnected.')
        return resp_payload

    def _term_device(self):
        if self._device is not None:
            self._device.stop()


class LEDsActuatorThread(MQTTActuatorThread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._color_options = ['AMBER', 'BLACK', 'GREEN', 'RED']
        self._left_color = None
        self._right_color = None

    def _init_device(self):
        self._device = Leds()

    def _process_request(self, req_payload):
        # get left led color
        left_color = req_payload.get('left_color')
        if not isinstance(left_color, str) or not left_color.upper() in self._color_options:
            resp_payload = dict(error='Invalid left_color.')
            return resp_payload

        # get right led color
        right_color = req_payload.get('right_color')
        if not isinstance(right_color, str) or not right_color.upper() in self._color_options:
            resp_payload = dict(error='Invalid right_color.')
            return resp_payload

        # update leds
        if self._left_color != left_color:
            self._left_color = left_color.upper()
            self._device.set_color('LEFT', self._left_color)
        if self._right_color != right_color:
            self._right_color = right_color.upper()
            self._device.set_color('RIGHT', self._right_color)
        resp_payload = dict(error=None)
        return resp_payload

    def _term_device(self):
        self._device.reset()


class PowerSensorThread(MQTTSensorThread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._device = None

    def _init_device(self):
        self._device = PowerSupply()

    def _create_message_payload(self):
        # check if device is connected and reinitialize if not
        if self._device is None:
            self._init_device()
            return None

        # create payload
        payload = dict()
        payload['current'] = self._device.measured_amps
        payload['voltage'] = self._device.measured_volts
        return payload

    def _term_device(self):
        pass


class SpeechActuatorThread(MQTTActuatorThread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _init_device(self):
        self._device = Sound()

    def _process_request(self, req_payload):
        text = req_payload.get('text')
        if not isinstance(text, str):
            resp_payload = dict(error='Invalid text.')
            return resp_payload

        # speak text
        self._device.speak(text, play_type=Sound.PLAY_WAIT_FOR_COMPLETE)
        resp_payload = dict(error=None)
        return resp_payload

    def _term_device(self):
        pass


class TouchSensorThread(MQTTSensorThread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._devices = list()

    def _init_device(self):
        # check for multiple touch sensors
        self._devices = list()
        for sensor in list_sensors(driver_name='lego-ev3-touch'):
            try:
                device = TouchSensor(sensor.address)
                self._devices.append(device)
            except DeviceNotFound:
                pass

    def _create_message_payload(self):
        # check if device is connected and reinitialize if not
        if len(self._devices) == 0:
            self._init_device()
            return None

        # create payload
        payload = None
        try:
            payload = dict()
            payload['touched'] = list()
            for device in self._devices:
                device.mode = 'TOUCH'
                payload['touched'].append(device.value())
            if len(payload['touched']) == 1:
                payload['touched'] = payload['touched'][0]
        except DeviceNotFound:
            # reinitialize device if not found
            self._init_device()
        return payload

    def _term_device(self):
        pass


class UltrasonicSensorThread(MQTTSensorThread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._device = None

    def _init_device(self):
        try:
            self._device = UltrasonicSensor()
        except DeviceNotFound:
            self._device = None

    def _create_message_payload(self):
        # check if device is connected and reinitialize if not
        if self._device is None:
            self._init_device()
            return None

        # create payload
        payload = None
        try:
            payload = dict()
            self._device.mode = 'US-DIST-CM'
            # note: convert to meters, but bug in code does not return cm
            # payload['range'] = self._ultrasonic_sensor.value() / 100
            payload['range'] = (self._device.value() / 10) / 100
        except DeviceNotFound:
            # reinitialize device if not found
            self._init_device()
        return payload

    def _term_device(self):
        pass


def main(args):
    try:
        # create mqtt manager
        mqtt_manager = MQTTManager(
            mqtt_ip=args.ip_address,
            mqtt_keepalive=args.keepalive,
            mqtt_port=args.port,
            verbose=args.verbose,
        )

        # execute mqtt manager
        try:
            mqtt_manager.run()
        except KeyboardInterrupt:
            print('')

        # terminate mqtt manager
        mqtt_manager.terminate()
    except FileNotFoundError:
        print('ERROR: Invalid EV3 device.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip-address', type=str, default='192.168.0.1')
    parser.add_argument('--keepalive', type=int, default=60)
    parser.add_argument('--port', type=int, default=1883)
    parser.add_argument('--silent', action='store_false', dest='verbose')
    main(parser.parse_args())

from queue import Queue
import rospy


class JetbotActuatorNode(object):
    def __init__(
            self,
            name='jetbot_actuator',
            topic=None,
            data_class=None,
            refresh_rate=15,
    ):
        self._topic = topic
        self._data_class = data_class
        self._refresh_rate = refresh_rate

        # initialize node
        rospy.init_node(name)
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # initialize subscriber
        self._sub_msg_queue = Queue()
        self._sub = None
        if self._topic is not None and self._data_class is not None:
            self._sub = rospy.Subscriber(self._topic, self._data_class, callback=self._message_cb)

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))
        rate = rospy.Rate(self._refresh_rate)
        while not rospy.is_shutdown():
            # get most recent message
            msg = None
            while self._sub_msg_queue.qsize() > 0:
                msg = self._sub_msg_queue.get(block=False)

            # process message
            if isinstance(msg, self._data_class):
                self._process_message(msg)

            # sleep until next iteration
            rate.sleep()

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        # unregister subscriber
        if self._sub is not None:
            self._sub.unregister()

    def _message_cb(self, msg):
        if isinstance(msg, self._data_class):
            self._sub_msg_queue.put(msg)

    def _process_message(self, sub_msg):
        raise NotImplementedError()

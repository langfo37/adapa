import json
import paho.mqtt.client as mqtt
import rospy
import uuid


class EV3MQTTActuatorNode(object):
    def __init__(
            self,
            mqtt_ip='192.168.0.1',
            mqtt_keepalive=60,
            mqtt_port=1883,
            mqtt_topic='ev3/actuator',
            name='ev3_actuator',
            topic=None,
            data_class=None,
    ):
        self._mqtt_client = None
        self._mqtt_ip = mqtt_ip
        self._mqtt_keepalive = mqtt_keepalive
        self._mqtt_port = mqtt_port
        self._mqtt_topic = mqtt_topic
        self._mqtt_request_topic = '{}/request'.format(self._mqtt_topic)
        self._mqtt_response_topic = '{}/response'.format(self._mqtt_topic)
        self._mqtt_request_counter = 0
        self._topic = topic
        self._data_class = data_class

        # initialize node
        rospy.init_node(name)
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # initialize subscriber
        self._sub = None
        if self._topic is not None and self._data_class is not None:
            self._sub = rospy.Subscriber(self._topic, self._data_class, self._message_cb)

        # init mqtt client
        self._init_mqtt_client()
        if self._mqtt_client is not None:
            self._mqtt_client.on_message = self._on_mqtt_message
            self._mqtt_client.subscribe(self._mqtt_response_topic)
            self._mqtt_client.loop_start()

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))

        # loop forever
        rospy.spin()

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        # terminate mqtt client
        if self._mqtt_client is not None:
            self._mqtt_client.loop_stop()
            self._term_mqtt_client()

        # unregister subscriber
        if self._sub is not None:
            self._sub.unregister()

    def _create_mqtt_payload(self, msg):
        raise NotImplementedError()

    def _init_mqtt_client(self):
        if self._mqtt_client is None:
            try:
                # create mqtt client and connect to broker
                client = mqtt.Client(protocol=mqtt.MQTTv311)
                client.connect(self._mqtt_ip, port=self._mqtt_port, keepalive=self._mqtt_keepalive)
                self._mqtt_client = client
                rospy.loginfo('MQTT client connected.')
            except OSError:
                rospy.logwarn('MQTT client connection failed.')

    def _message_cb(self, msg):
        if isinstance(msg, self._data_class):
            # create mqtt payload
            mqtt_payload = self._create_mqtt_payload(msg)
            mqtt_payload['_uuid'] = str(uuid.uuid4())

            # publish to mqtt topic
            if self._mqtt_client is not None:
                self._mqtt_client.publish(self._mqtt_request_topic, json.dumps(mqtt_payload))

    def _on_mqtt_message(self, client, userdata, msg):
        if msg.topic == self._mqtt_response_topic:
            mqtt_payload = json.loads(str(msg.payload.decode('utf-8', 'ignore')))
            uuid = mqtt_payload.get('_uuid')

    def _term_mqtt_client(self):
        if self._mqtt_client is not None:
            # disconnect mqtt client
            self._mqtt_client.disconnect()
            rospy.loginfo('MQTT client disconnected.')
        self._mqtt_client = None

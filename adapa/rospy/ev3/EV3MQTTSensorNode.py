from rospy.exceptions import ROSException
import json
import paho.mqtt.client as mqtt
import rospy


class EV3MQTTSensorNode(object):
    def __init__(
            self,
            mqtt_ip='192.168.0.1',
            mqtt_keepalive=60,
            mqtt_port=1883,
            mqtt_topic='ev3/sensor',
            name='ev3_sensor',
            topic=None,
            data_class=None,
    ):
        self._mqtt_client = None
        self._mqtt_ip = mqtt_ip
        self._mqtt_keepalive = mqtt_keepalive
        self._mqtt_port = mqtt_port
        self._mqtt_topic = mqtt_topic
        self._topic = topic
        self._data_class = data_class

        # initialize node
        rospy.init_node(name)
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # initialize publisher
        self._pub = None
        if self._topic is not None and self._data_class is not None:
            self._pub = rospy.Publisher(self._topic, self._data_class, queue_size=30)

        # init mqtt client
        self._init_mqtt_client()
        if self._mqtt_client is not None:
            self._mqtt_client.on_message = self._on_mqtt_message
            self._mqtt_client.subscribe(self._mqtt_topic)
            self._mqtt_client.loop_start()

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))

        # loop forever
        rospy.spin()

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        # terminate mqtt client
        if self._mqtt_client is not None:
            self._mqtt_client.loop_stop()
            self._term_mqtt_client()

        # unregister publisher
        if self._pub is not None:
            self._pub.unregister()

    def _create_message(self, mqtt_payload):
        raise NotImplementedError()

    def _init_mqtt_client(self):
        if self._mqtt_client is None:
            try:
                # create mqtt client and connect to broker
                client = mqtt.Client(protocol=mqtt.MQTTv311)
                client.connect(self._mqtt_ip, port=self._mqtt_port, keepalive=self._mqtt_keepalive)
                self._mqtt_client = client
                rospy.loginfo('MQTT client connected.')
            except OSError:
                rospy.logwarn('MQTT client connection failed.')

    def _on_mqtt_message(self, client, userdata, msg):
        if msg.topic == self._mqtt_topic:
            mqtt_payload = json.loads(str(msg.payload.decode('utf-8', 'ignore')))
            msg = self._create_message(mqtt_payload)
            if isinstance(msg, self._data_class):
                try:
                    self._pub.publish(msg)
                except ROSException:
                    pass

    def _term_mqtt_client(self):
        if self._mqtt_client is not None:
            # disconnect mqtt client
            self._mqtt_client.disconnect()
            rospy.loginfo('MQTT client disconnected.')
        self._mqtt_client = None

from queue import Queue
import rospy


class GazeboActuatorNode(object):
    def __init__(
            self,
            name='gazebo_actuator',
            topic=None,
            data_class=None,
            gazebo_topic=None,
            gazebo_data_class=None,
            refresh_rate=15,
    ):
        self._topic = topic
        self._data_class = data_class
        self._gazebo_topic = gazebo_topic
        self._gazebo_data_class = gazebo_data_class
        self._refresh_rate = refresh_rate

        # initialize node
        rospy.init_node(name)
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # initialize subscriber
        self._sub_msg_queue = Queue()
        self._sub = None
        if self._topic is not None and self._data_class is not None:
            self._sub = rospy.Subscriber(self._topic, self._data_class, callback=self._message_cb)

        # initialize publisher
        self._pub = None
        if self._gazebo_topic is not None and self._gazebo_data_class is not None:
            self._pub = rospy.Publisher(self._gazebo_topic, self._gazebo_data_class, queue_size=1)

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))
        rate = rospy.Rate(self._refresh_rate)
        while not rospy.is_shutdown():
            # get most recent message
            msg = None
            while self._sub_msg_queue.qsize() > 0:
                msg = self._sub_msg_queue.get(block=False)

            # convert to gazebo message and publish
            if isinstance(msg, self._data_class):
                gazebo_msg = self._convert_message(msg)
                if isinstance(gazebo_msg, self._gazebo_data_class):
                    self._pub.publish(gazebo_msg)

            # sleep until next iteration
            rate.sleep()

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        # unregister subscriber
        if self._sub is not None:
            self._sub.unregister()

        # unregister publisher
        if self._pub is not None:
            self._pub.unregister()

    def _convert_message(self, msg):
        raise NotImplementedError()

    def _message_cb(self, sub_msg):
        if isinstance(sub_msg, self._data_class):
            self._sub_msg_queue.put(sub_msg)

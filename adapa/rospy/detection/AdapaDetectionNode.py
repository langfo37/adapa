from adapa_ros.msg import AdapaDetectionMessage
from adapa_ros.msg import JetbotCameraMessage
from adapa_ros.srv import AdapaDetectionModelChangeService
from adapa_ros.srv._AdapaDetectionModelChangeService import AdapaDetectionModelChangeServiceRequest
from adapa_ros.srv import AdapaDetectionModelChangeServiceResponse
from anunnaki_torch.adapa_detection.backend.dnn import AdapaDetectionDNN
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import MultiArrayDimension
from std_msgs.msg import UInt8MultiArray
from PIL import Image
from queue import Queue
import io
import numpy as np
import os
import rospy
import torch


class AdapaDetectionNode(object):
    def __init__(
            self,
            name='adapa_detection',
            pub_data_class=AdapaDetectionMessage,
            pub_topic='/adapa/sensor/detection',
            sub_data_class=JetbotCameraMessage,
            sub_topic='/jetbot/sensor/camera',
            model_path=None,
            refresh_rate=15,
    ):
        self._pub_topic = pub_topic
        self._pub_data_class = pub_data_class
        self._sub_topic = sub_topic
        self._sub_data_class = sub_data_class
        self._model_path = model_path
        self._refresh_rate = refresh_rate

        # initialize node
        rospy.init_node(name)
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # initialize publisher
        self._pub = None
        if self._pub_topic is not None and self._pub_data_class is not None:
            self._pub = rospy.Publisher(self._pub_topic, self._pub_data_class, queue_size=10)

        # initialize subscriber
        self._sub_msg_queue = Queue()
        self._sub = None
        if self._sub_topic is not None and self._sub_data_class is not None:
            self._sub = rospy.Subscriber(self._sub_topic, self._sub_data_class, callback=self._message_cb)

        # initialize services
        self._srvs = dict(
            mode_change_service=rospy.Service('/adapa/detection/model_change', AdapaDetectionModelChangeService, self._request_model_change)
        )

        # initialize request queue
        self._request_queue = Queue()

        # load dnn
        self._device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self._load_dnn(model_path)

    @staticmethod
    def detections_to_message(detections, image_size=None):
        if not isinstance(detections, list):
            return None

        # create multiarray of detection types
        detection_types_ndarray = np.array([entry['type'] for entry in detections], dtype=np.uint8)
        detection_types_multiarray = UInt8MultiArray()
        detection_types_multiarray.layout.dim = [
            MultiArrayDimension(
                'dim{:d}'.format(i),
                detection_types_ndarray.shape[i],
                detection_types_ndarray.shape[i] * detection_types_ndarray.dtype.itemsize,
            )
            for i in range(detection_types_ndarray.ndim)
        ]
        detection_types_multiarray.data = detection_types_ndarray.reshape([1, -1])[0].tolist()

        # create multiarray of detection boxes
        detection_boxes_ndarray = np.array([entry['box'] for entry in detections], dtype=np.float32)
        if isinstance(image_size, (tuple, list)) and len(detection_boxes_ndarray) > 0:
            # scale boxes to unit range
            detection_boxes_ndarray[:, 0] /= image_size[0]
            detection_boxes_ndarray[:, 1] /= image_size[1]
            detection_boxes_ndarray[:, 2] /= image_size[0]
            detection_boxes_ndarray[:, 3] /= image_size[1]
        detection_boxes_multiarray = Float32MultiArray()
        detection_boxes_multiarray.layout.dim = [
            MultiArrayDimension(
                'dim{:d}'.format(i),
                detection_boxes_ndarray.shape[i],
                detection_boxes_ndarray.shape[i] * detection_boxes_ndarray.dtype.itemsize,
            )
            for i in range(detection_boxes_ndarray.ndim)
        ]
        detection_boxes_multiarray.data = detection_boxes_ndarray.reshape([1, -1])[0].tolist()

        # create multiarray of detection scores
        detection_scores_ndarray = np.array([entry['score'] for entry in detections], dtype=np.float32)
        detection_scores_multiarray = Float32MultiArray()
        detection_scores_multiarray.layout.dim = [
            MultiArrayDimension(
                'dim{:d}'.format(i),
                detection_scores_ndarray.shape[i],
                detection_scores_ndarray.shape[i] * detection_scores_ndarray.dtype.itemsize,
            )
            for i in range(detection_scores_ndarray.ndim)
        ]
        detection_scores_multiarray.data = detection_scores_ndarray.reshape([1, -1])[0].tolist()

        # create message
        msg = AdapaDetectionMessage()
        msg.header.stamp = rospy.Time.now()
        msg.types = detection_types_multiarray
        msg.boxes = detection_boxes_multiarray
        msg.scores = detection_scores_multiarray
        return msg

    @staticmethod
    def message_to_detections(msg):
        if not isinstance(msg, AdapaDetectionMessage):
            return None

        # create ndarray of detection types
        if isinstance(msg.types.data, bytes):
            detection_types = np.frombuffer(msg.types.data, dtype=np.uint8)
        else:
            detection_types = np.array(msg.types.data, dtype=np.uint8)
        detection_types_shape = tuple(x.size for x in msg.types.layout.dim)
        detection_types = detection_types.reshape(detection_types_shape)

        # create ndarray of detection boxes
        if isinstance(msg.boxes.data, bytes):
            detection_boxes = np.frombuffer(msg.boxes.data, dtype=np.float32)
        else:
            detection_boxes = np.array(msg.boxes.data, dtype=np.float32)
        detection_boxes_shape = tuple(x.size for x in msg.boxes.layout.dim)
        detection_boxes = detection_boxes.reshape(detection_boxes_shape)

        # create ndarray of detection scores
        if isinstance(msg.scores.data, bytes):
            detection_scores = np.frombuffer(msg.scores.data, dtype=np.float32)
        else:
            detection_scores = np.array(msg.scores.data, dtype=np.float32)
        detection_scores_shape = tuple(x.size for x in msg.scores.layout.dim)
        detection_scores = detection_scores.reshape(detection_scores_shape)

        # create list of detections
        detections = [
            {'type': d_type, 'box': d_box, 'score': d_score}
            for d_type, d_box, d_score in zip(detection_types, detection_boxes, detection_scores)
        ]
        return detections

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))
        rate = rospy.Rate(self._refresh_rate)
        while not rospy.is_shutdown():
            # get newest message
            sub_msg = None
            while self._sub_msg_queue.qsize() > 0:
                sub_msg = self._sub_msg_queue.get(block=False)

            # process pending requests
            while self._request_queue.qsize() > 0:
                req_msg = self._request_queue.get(block=False)
                if isinstance(req_msg, AdapaDetectionModelChangeServiceRequest):
                    # process model change request
                    model_path = req_msg.model_path
                    self._load_dnn(model_path)

            # process message
            self._process_message(sub_msg)

            rate.sleep()

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        # unregister publisher
        if self._pub is not None:
            self._pub.unregister()

        # unregister subscriber
        if self._sub is not None:
            self._sub.unregister()

    def _load_dnn(self, model_path):
        if isinstance(model_path, str) and os.path.exists(os.path.expanduser(model_path)):
            model_path = os.path.expanduser(model_path)
            rospy.loginfo('Loading DNN from "{}".'.format(model_path))
            self._dnn = AdapaDetectionDNN.read(model_path, device=self._device, verbose=False)
            rospy.loginfo('DNN initialized.')
            self._model_path = model_path
        else:
            self._dnn = None
            rospy.logwarn('Failed to load DNN from "{}".'.format(model_path))

    def _message_cb(self, sub_msg):
        if isinstance(sub_msg, self._sub_data_class):
            self._sub_msg_queue.put(sub_msg)

    def _process_message(self, sub_msg):
        if isinstance(sub_msg, self._sub_data_class):
            # convert image
            image = Image.open(io.BytesIO(sub_msg.image.data))

            # process image with dnn
            detections = list()
            if self._dnn is not None:
                detections = self._dnn.process(image)

            # publish message
            if self._pub is not None:
                pub_msg = self.detections_to_message(detections, image_size=image.size)
                self._pub.publish(pub_msg)

    def _request_model_change(self, req_msg):
        # queue request and return response
        self._request_queue.put(req_msg)
        return AdapaDetectionModelChangeServiceResponse(status='')

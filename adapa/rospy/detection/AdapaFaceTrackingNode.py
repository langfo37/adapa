from adapa_ros.msg import AdapaFaceTrackingMessage
from adapa_ros.msg import UserCameraMessage
from PIL import Image
from queue import Queue
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import MultiArrayDimension
import io
import numpy as np
import os
import rospy

# One millisecond face alignment with an ensemble of regression trees
# V. Kazemi, J. Sullivan
# ref: https://www.semanticscholar.org/paper/One-millisecond-face-alignment-with-an-ensemble-of-Kazemi-Sullivan/d78b6a5b0dcaa81b1faea5fb0000045a62513567
# ref: https://www.pyimagesearch.com/2019/12/16/training-a-custom-dlib-shape-predictor/
# ref: https://medium.com/analytics-vidhya/real-time-head-pose-estimation-with-opencv-and-dlib-e8dc10d62078


class AdapaFaceTrackingNode(object):
    def __init__(
            self,
            name='adapa_facetracking',
            pub_data_class=AdapaFaceTrackingMessage,
            pub_topic='/adapa/sensor/facetracking',
            sub_data_class=UserCameraMessage,
            sub_topic='/user/sensor/camera',
            model_path=None,
            refresh_rate=15,
    ):
        self._pub_topic = pub_topic
        self._pub_data_class = pub_data_class
        self._sub_topic = sub_topic
        self._sub_data_class = sub_data_class
        self._model_path = model_path
        self._refresh_rate = refresh_rate

        # initialize node
        rospy.init_node(name)
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # initialize publisher
        self._pub = None
        if self._pub_topic is not None and self._pub_data_class is not None:
            self._pub = rospy.Publisher(self._pub_topic, self._pub_data_class, queue_size=10)

        # initialize subscriber
        self._sub_msg_queue = Queue()
        self._sub = None
        if self._sub_topic is not None and self._sub_data_class is not None:
            self._sub = rospy.Subscriber(self._sub_topic, self._sub_data_class, callback=self._message_cb)

        try:
            import cv2
            import dlib
            if isinstance(self._model_path, str) and os.path.exists(os.path.expanduser(self._model_path)):
                self._model_path = os.path.expanduser(model_path)
                rospy.loginfo('Loading face landmarks from "{}".'.format(self._model_path))
                self._detector = dlib.get_frontal_face_detector()
                self._predictor = dlib.shape_predictor(self._model_path)
                rospy.loginfo('Face detector initialized.')
            else:
                self._predictor = None
                rospy.logwarn('Failed to load face landmarks from "{}".'.format(model_path))
        except ImportError:
            rospy.loginfo('Failed to initialize face detector.')
            self._detector = None
            self._predictor = None

    @staticmethod
    def detections_to_message(face_landmarks, euler_angles, axis_points):
        # create multiarray of face markers
        if not isinstance(face_landmarks, np.ndarray):
            return None
        face_landmarks_multiarray = Float32MultiArray()
        face_landmarks_multiarray.layout.dim = [
            MultiArrayDimension(
                'dim{:d}'.format(i),
                face_landmarks.shape[i],
                face_landmarks.shape[i] * face_landmarks.dtype.itemsize,
            )
            for i in range(face_landmarks.ndim)
        ]
        face_landmarks_multiarray.data = face_landmarks.reshape([1, -1])[0].tolist()

        # create multiarray of euler angles
        if not isinstance(euler_angles, np.ndarray):
            return None
        euler_angles_multiarray = Float32MultiArray()
        euler_angles_multiarray.layout.dim = [
            MultiArrayDimension(
                'dim{:d}'.format(i),
                euler_angles.shape[i],
                euler_angles.shape[i] * euler_angles.dtype.itemsize,
            )
            for i in range(euler_angles.ndim)
        ]
        euler_angles_multiarray.data = euler_angles.reshape([1, -1])[0].tolist()

        # create multiarray of gaze projection
        if not isinstance(axis_points, np.ndarray):
            return None
        axis_points_multiarray = Float32MultiArray()
        axis_points_multiarray.layout.dim = [
            MultiArrayDimension(
                'dim{:d}'.format(i),
                axis_points.shape[i],
                axis_points.shape[i] * axis_points.dtype.itemsize,
            )
            for i in range(axis_points.ndim)
        ]
        axis_points_multiarray.data = axis_points.reshape([1, -1])[0].tolist()

        # create message
        msg = AdapaFaceTrackingMessage()
        msg.header.stamp = rospy.Time.now()
        msg.face_landmarks = face_landmarks_multiarray
        msg.euler_angles = euler_angles_multiarray
        msg.axis_points = axis_points_multiarray
        return msg

    @staticmethod
    def message_to_detections(msg):
        if not isinstance(msg, AdapaFaceTrackingMessage):
            return None

        # create ndarray of face markers
        if isinstance(msg.face_landmarks.data, bytes):
            face_landmarks = np.frombuffer(msg.face_landmarks.data, dtype=np.float32)
        else:
            face_landmarks = np.array(msg.face_landmarks.data, dtype=np.float32)
        face_landmarks_shape = tuple(x.size for x in msg.face_landmarks.layout.dim)
        face_landmarks = face_landmarks.reshape(face_landmarks_shape)
        
        # create ndarray of euler angles
        if isinstance(msg.euler_angles.data, bytes):
            euler_angles = np.frombuffer(msg.euler_angles.data, dtype=np.float32)
        else:
            euler_angles = np.array(msg.euler_angles.data, dtype=np.float32)
        euler_angles_shape = tuple(x.size for x in msg.euler_angles.layout.dim)
        euler_angles = euler_angles.reshape(euler_angles_shape)

        # create ndarray of gaze line
        if isinstance(msg.axis_points.data, bytes):
            axis_points = np.frombuffer(msg.axis_points.data, dtype=np.float32)
        else:
            axis_points = np.array(msg.axis_points.data, dtype=np.float32)
        axis_points_shape = tuple(x.size for x in msg.axis_points.layout.dim)
        axis_points = axis_points.reshape(axis_points_shape)

        return face_landmarks, euler_angles, axis_points

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))
        rate = rospy.Rate(self._refresh_rate)
        while not rospy.is_shutdown():
            # get newest message
            sub_msg = None
            while self._sub_msg_queue.qsize() > 0:
                sub_msg = self._sub_msg_queue.get(block=False)

            # process message
            self._process_message(sub_msg)

            rate.sleep()

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        # unregister publisher
        if self._pub is not None:
            self._pub.unregister()

        # unregister subscriber
        if self._sub is not None:
            self._sub.unregister()

    def _message_cb(self, sub_msg):
        if isinstance(sub_msg, self._sub_data_class):
            self._sub_msg_queue.put(sub_msg)

    def _process_message(self, sub_msg):
        try:
            # attempt to import opencv and dlib
            import cv2
        except ImportError:
            return

        if isinstance(sub_msg, self._sub_data_class):
            # convert image
            image = Image.open(io.BytesIO(sub_msg.image.data))
            image = np.array(image.convert('RGB'))
            image_gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

            # detect faces and focus on largest face
            face_boxes = self._detector(image_gray)
            face_box = max(face_boxes, key=lambda rect: rect.width() * rect.height()) if len(face_boxes) > 0 else None

            # locate face landmarks and estimate pose
            face_landmarks = np.zeros((68, 2), dtype=np.float32)
            euler_angles = np.zeros(3, dtype=np.float32)
            axis_points = np.zeros((4, 1, 2), dtype=np.float32)
            if face_box is not None:
                # predict face shape
                face_shape = self._predictor(image_gray, face_box)

                # get face landmark coords
                for i in range(len(face_landmarks)):
                    face_landmarks[i] = (face_shape.part(i).x, face_shape.part(i).y)
                face_landmarks[:, 0] /= image.shape[1]
                face_landmarks[:, 1] /= image.shape[0]

                # define face 2d coord reference
                image_points = np.float32([
                    [face_shape.part(30).x, face_shape.part(30).y],
                    [face_shape.part(8).x, face_shape.part(8).y],
                    [face_shape.part(36).x, face_shape.part(36).y],
                    [face_shape.part(45).x, face_shape.part(45).y],
                    [face_shape.part(48).x, face_shape.part(48).y],
                    [face_shape.part(54).x, face_shape.part(54).y],
                ])

                # define face 3d coord reference
                object_points = np.float32([
                    [0.0, 0.0, 0.0],
                    [0.0, -330.0, -65.0],
                    [-225.0, 170.0, -135.0],
                    [225.0, 170.0, -135.0],
                    [-150.0, -150.0, -125.0],
                    [150.0, -150.0, -125.0],
                ])

                # define camera matrix
                focal = (0.024 / 0.0362) * image.shape[1]
                camera_matrix = np.float32([
                    [focal, 1, image.shape[0] / 2],
                    [0, focal, image.shape[1] / 2],
                    [0, 0, 1],
                ])

                # calculate rotation and translation vector using solvePnP
                dist_coeffs = np.zeros((4, 1), dtype=np.float64)
                retval, rvec, tvec = cv2.solvePnP(object_points, image_points, camera_matrix, dist_coeffs)

                # calculate euler angles
                rmat, jac = cv2.Rodrigues(rvec)
                angles, mtx_r, mtx_q, q_x, q_y, q_z = cv2.RQDecomp3x3(rmat)
                euler_angles[:] = angles

                # calculate pose coordinate frame
                axis_points = np.float32([[200, 0, 0], [0, 200, 0], [0, 0, 200], [0, 0, 0]]).reshape(-1, 3)
                axis_points, jacobian = cv2.projectPoints(axis_points, rvec, tvec, camera_matrix, dist_coeffs)
                axis_points[:, :, 0] += (face_shape.part(28).x - face_shape.part(30).x)
                axis_points[:, :, 1] += (face_shape.part(28).y - face_shape.part(30).y)

            # publish message
            if self._pub is not None:
                pub_msg = self.detections_to_message(face_landmarks, euler_angles, axis_points)
                if pub_msg is not None:
                    self._pub.publish(pub_msg)

from queue import Queue
import rospy
import threading


class TopicListener(object):
    def __init__(self, topic, data_class, window_size=100):
        self._topic = topic
        self._data_class = data_class
        self._window_size = window_size if window_size >= 0 else 50000
        self._lock = threading.Lock()
        self._msg_queue = Queue()
        self._msg_t0 = -1.0
        self._msg_tn = 0.0
        self._last_tn = 0.0
        self._times = list()

    @property
    def data_class(self):
        return self._data_class

    @property
    def topic(self):
        return self._topic

    def callback(self, msg):
        if not isinstance(msg, self._data_class):
            return

        # add message to queue and truncate to window size
        self._msg_queue.put(msg)
        while self._msg_queue.qsize() > self._window_size:
            self._msg_queue.get(block=False)
        with self._lock:
            # reset counters when time resets
            curr_time = rospy.Time.now()
            if curr_time.is_zero():
                self._times = list()
                return

            # update time records
            curr_time = curr_time.to_sec()
            if self._msg_t0 < 0 or self._msg_t0 > curr_time:
                # initialize records
                self._msg_t0 = curr_time
                self._msg_tn = curr_time
                self._times = list()
            else:
                # add entry to records
                self._times.append(curr_time - self._msg_tn)
                self._msg_tn = curr_time

            # truncate records
            if len(self._times) > self._window_size - 1:
                self._times.pop(0)

    def compute_elapsed(self):
        curr_time = rospy.Time.now().to_sec()
        return curr_time - self._msg_tn

    def compute_rate(self):
        rate = 0.0
        if self._times:
            with self._lock:
                mean = sum(self._times) / len(self._times)
                rate = 1.0 / mean if mean > 0.0 else 0.0
                self._last_tn = self._msg_tn
        return rate

    def fetch_newest_message(self):
        msg = None
        while self._msg_queue.qsize() > 0:
            msg = self._msg_queue.get(block=False)
        return msg

    def fetch_next_message(self):
        msg = None
        if self._msg_queue.qsize() > 0:
            msg = self._msg_queue.get(block=False)
        return msg

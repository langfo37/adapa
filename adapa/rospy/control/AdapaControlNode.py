from adapa.rospy.detection.AdapaDetectionNode import AdapaDetectionNode
from adapa.rospy.util.TopicListener import TopicListener
from adapa_ros.msg import AdapaControlMessage
from adapa_ros.msg import AdapaDetectionMessage
from adapa_ros.msg import EV3ArmMessage
from adapa_ros.msg import EV3BatteryMessage
from adapa_ros.msg import EV3HandMessage
from adapa_ros.msg import EV3TouchMessage
from adapa_ros.msg import EV3UltrasonicMessage
from adapa_ros.msg import JetbotBatteryMessage
from adapa_ros.msg import JetbotBuzzerMessage
from adapa_ros.msg import JetbotLightMessage
from adapa_ros.msg import JetbotDriveMessage
from adapa_ros.msg import JetbotServosMessage
from adapa_ros.srv import AdapaModeChangeService
from adapa_ros.srv._AdapaModeChangeService import AdapaModeChangeServiceRequest
from adapa_ros.srv import AdapaModeChangeServiceResponse
from queue import Queue
from sensor_msgs.msg import Joy
import rospy
import threading
import time


class AdapaControlNode(object):
    def __init__(self, mode='manual', refresh_rate=15, stale_threshold=1.0):
        self._refresh_rate = refresh_rate
        self._stale_threshold = stale_threshold

        # create threading lock
        self._lock = threading.Lock()

        # initialize node
        rospy.init_node('control')
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # initialize publishers
        self._pubs = dict(
            adapa_control_topic=rospy.Publisher('/adapa/control/state', AdapaControlMessage, queue_size=1),
            ev3_arm_topic=rospy.Publisher('/ev3/actuator/arm', EV3ArmMessage, queue_size=1),
            ev3_hand_topic=rospy.Publisher('/ev3/actuator/hand', EV3HandMessage, queue_size=1),
            jetbot_buzzer_topic=rospy.Publisher('/jetbot/actuator/buzzer', JetbotBuzzerMessage, queue_size=1),
            jetbot_drive_topic=rospy.Publisher('/jetbot/actuator/drive', JetbotDriveMessage, queue_size=1),
            jetbot_lights_topic=rospy.Publisher('/jetbot/actuator/lights', JetbotLightMessage, queue_size=1),
            jetbot_servos_topic=rospy.Publisher('/jetbot/actuator/servos', JetbotServosMessage, queue_size=1),
        )

        # initialize subscribers
        self._sub_listeners = dict(
            adapa_detection_topic=TopicListener('/adapa/sensor/detection', AdapaDetectionMessage),
            ev3_battery_topic=TopicListener('/ev3/sensor/battery', EV3BatteryMessage),
            ev3_touch_topic=TopicListener('/ev3/sensor/touch', EV3TouchMessage),
            ev3_ultrasonic_topic=TopicListener('/ev3/sensor/ultrasonic', EV3UltrasonicMessage),
            jetbot_battery_topic=TopicListener('/jetbot/sensor/battery', JetbotBatteryMessage),
            joy_topic=TopicListener('/joy', Joy),
        )
        self._subs = {
            key: rospy.Subscriber(manager.topic, manager.data_class, callback=manager.callback)
            for key, manager in self._sub_listeners.items()
        }

        # initialize services
        self._srvs = dict(
            mode_change_service=rospy.Service('/adapa/control/mode_change', AdapaModeChangeService, self._request_mode_change)
        )

        # initialize request queue
        self._request_queue = Queue()

        # initialize percepts
        self._percepts = {key: None for key, manager in self._sub_listeners.items()}

        # initialize state
        self._state = dict(
            collision=False,
            detected_deer=False,
            detected_focus_size=0,
            detected_focus_x=0,
            detected_focus_y=0,
            detected_human=False,
            detection_stale=True,
            ev3_arm='down',
            ev3_battery_voltage=0.0,
            ev3_battery_status='',
            ev3_hand='closed',
            ev3_ultrasonic_range=0.0,
            ev3_ultrasonic_range_max=0.0,
            ev3_ultrasonic_stale=True,
            jetbot_battery_voltage=0.0,
            jetbot_battery_status='',
            jetbot_buzzer=False,
            jetbot_cam_reset=False,
            jetbot_cam_rx=0,
            jetbot_cam_rz=0,
            jetbot_drive_dx=0.0,
            jetbot_drive_rz=0.0,
            jetbot_lights='#000000',
            mode=mode,
            n_collisions=0,
        )

        # mode settings
        self._mode_options = ['auto', 'manual']
        self._mode_toggle_time = time.time()
        if self._state['mode'] not in self._mode_options:
            raise ValueError('Invalid mode: "{}".'.format(self._state['mode']))

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))
        rate = rospy.Rate(self._refresh_rate)
        while not rospy.is_shutdown():
            with self._lock:
                # get most current messages from percept subscribers
                for key, manager in self._sub_listeners.items():
                    msg = manager.fetch_newest_message()
                    if key in self._percepts and msg is not None:
                        self._percepts[key] = msg

                # process pending requests
                while self._request_queue.qsize() > 0:
                    req_msg = self._request_queue.get(block=False)
                    if isinstance(req_msg, AdapaModeChangeServiceRequest):
                        # process mode change request
                        mode = req_msg.mode.lower()
                        if mode in self._mode_options:
                            self._mode_toggle_time = time.time()
                            self._state['mode'] = mode
                        rospy.loginfo('Mode change request to "{}".'.format(mode))

                # check sensors
                self._check_battery()
                self._check_collision()
                self._check_detection()
                self._check_mode()
                self._check_ultrasonic()

                # drive according to mode
                actions = None
                if self._state['mode'] == 'auto':
                    # determine actions according to autonomous mode
                    actions = self._drive_auto()
                elif self._state['mode'] == 'manual':
                    # determine actions according to manual mode
                    actions = self._drive_manual()

                # update actuators
                if actions is not None:
                    self._update_arm(actions)
                    self._update_buzzer(actions)
                    self._update_drive(actions)
                    self._update_hand(actions)
                    self._update_lights(actions)
                    self._update_servos(actions)

                # publish current state
                control_msg = AdapaControlMessage()
                control_msg.header.stamp = rospy.Time.now()
                for k, v in self._state.items():
                    if hasattr(control_msg, k):
                        setattr(control_msg, k, v)
                self._pubs['adapa_control_topic'].publish(control_msg)

                # erase joystick data from percepts
                self._percepts['joy_topic'] = None

            # sleep until next iteration
            rate.sleep()

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        # unregister publishers
        for key, pub in self._pubs.items():
            pub.unregister()

        # unregister subscribers
        for key, sub in self._subs.items():
            sub.unregister()

    def _check_battery(self):
        # get jetbot battery status
        jetbot_battery_elapsed = self._sub_listeners['jetbot_battery_topic'].compute_elapsed()
        jetbot_battery_msg = self._percepts['jetbot_battery_topic']
        if jetbot_battery_elapsed < self._stale_threshold and isinstance(jetbot_battery_msg, JetbotBatteryMessage):
            self._state['jetbot_battery_voltage'] = jetbot_battery_msg.voltage
            self._state['jetbot_battery_status'] = jetbot_battery_msg.status

        # get ev3 battery status
        ev3_battery_elapsed = self._sub_listeners['ev3_battery_topic'].compute_elapsed()
        ev3_battery_msg = self._percepts['ev3_battery_topic']
        if ev3_battery_elapsed < self._stale_threshold and isinstance(ev3_battery_msg, EV3BatteryMessage):
            self._state['ev3_battery_voltage'] = ev3_battery_msg.voltage
            self._state['ev3_battery_status'] = ev3_battery_msg.status

    def _check_collision(self):
        touch_elapsed = self._sub_listeners['ev3_touch_topic'].compute_elapsed()
        touch_msg = self._percepts['ev3_touch_topic']
        if touch_elapsed < self._stale_threshold and isinstance(touch_msg, EV3TouchMessage):
            if touch_msg.touched:
                # set collision flag and increment collision counter if new collision
                if not self._state['collision']:
                    self._state['collision'] = True
                    self._state['n_collisions'] += 1
            else:
                # reset collision flag
                self._state['collision'] = False

    def _check_detection(self):
        detection_elapsed = self._sub_listeners['adapa_detection_topic'].compute_elapsed()
        detection_msg = self._percepts['adapa_detection_topic']
        if detection_elapsed < self._stale_threshold and isinstance(detection_msg, AdapaDetectionMessage):
            detected_deer = False
            detected_human = False
            detected_focus_size = 0
            detected_focus_x = 0
            detected_focus_y = 0
            detections = AdapaDetectionNode.message_to_detections(detection_msg)
            for i, detection in enumerate(detections):
                if detection['type'] == 1:
                    detected_human = True
                if detection['type'] == 2:
                    detected_deer = True
                if detection['type'] != 0:
                    curr_size = (detection['box'][2] - detection['box'][0]) * (detection['box'][3] - detection['box'][1])
                    if curr_size > detected_focus_size:
                        detected_focus_size = curr_size
                        detected_focus_x = detection['box'][0] + (detection['box'][2] - detection['box'][0]) / 2
                        detected_focus_y = detection['box'][1] + (detection['box'][3] - detection['box'][1]) / 2
            self._state['detected_deer'] = detected_deer
            self._state['detected_human'] = detected_human
            self._state['detected_focus_size'] = detected_focus_size
            self._state['detected_focus_x'] = detected_focus_x
            self._state['detected_focus_y'] = detected_focus_y
        self._state['detection_stale'] = detection_elapsed >= self._stale_threshold

    def _check_mode(self):
        joy_elapsed = self._sub_listeners['joy_topic'].compute_elapsed()
        joy_msg = self._percepts['joy_topic']
        if joy_elapsed < self._stale_threshold and isinstance(joy_msg, Joy):
            if joy_msg.buttons[6]:
                # force a toggle delay
                elapsed = time.time() - self._mode_toggle_time
                if elapsed > 0.1:
                    # cycle to next mode option
                    self._mode_toggle_time = time.time()
                    curr_mode = self._state['mode']
                    i = (self._mode_options.index(curr_mode) + 1) % len(self._mode_options)
                    self._state['mode'] = self._mode_options[i]

    def _check_ultrasonic(self):
        # get ev3 ultrasonic status
        ultrasonic_elapsed = self._sub_listeners['ev3_ultrasonic_topic'].compute_elapsed()
        ultrasonic_msg = self._percepts['ev3_ultrasonic_topic']
        if ultrasonic_elapsed < self._stale_threshold and isinstance(ultrasonic_msg, EV3UltrasonicMessage):
            self._state['ev3_ultrasonic_range'] = ultrasonic_msg.range.range
            self._state['ev3_ultrasonic_range_max'] = ultrasonic_msg.range.max_range
        self._state['ev3_ultrasonic_stale'] = ultrasonic_elapsed >= self._stale_threshold

    def _drive_auto(self):
        actions = dict()

        # check if detection data is stale
        if self._state['detection_stale']:
            # buzzer off, lights yellow, stop driving
            rospy.logwarn('Aborting auto drive due to stale detections data.')
            actions['jetbot_buzzer_status'] = False
            actions['jetbot_lights_color'] = '#ffff00'
            actions['jetbot_drive_dx'] = 0.0
            actions['jetbot_drive_rz'] = 0.0
            return actions

        # check if ultrasonic data is stale
        if self._state['ev3_ultrasonic_stale']:
            # buzzer off, lights yellow, stop driving
            rospy.logwarn('Aborting auto drive due to stale ultrasonic data.')
            actions['jetbot_buzzer_status'] = False
            actions['jetbot_lights_color'] = '#ffff00'
            actions['jetbot_drive_dx'] = 0.0
            actions['jetbot_drive_rz'] = 0.0
            return actions

        # check if in a collision state
        if self._state['collision']:
            # buzzer on, lights red, stop driving
            actions['jetbot_buzzer_status'] = True
            actions['jetbot_lights_color'] = '#ff0000'
            actions['jetbot_drive_dx'] = 0.0
            actions['jetbot_drive_rz'] = 0.0
            return actions

        # get detection status
        detected_human = self._state['detected_human']
        detected_focus_size = self._state['detected_focus_size']
        detected_focus_x = self._state['detected_focus_x']

        # get distance from ultrasonic sensor
        range_distance = self._state['ev3_ultrasonic_range']
        range_max = self._state['ev3_ultrasonic_range_max']

        if detected_focus_size >= 0.05 and 0 < detected_focus_x <= 0.5:
            # buzzer on (if human), lights yellow (if human), turn right
            actions['jetbot_buzzer_status'] = True if detected_human else False
            actions['jetbot_lights_color'] = '#ffff00' if detected_human else '#00ff00'
            actions['jetbot_drive_dx'] = 0.0
            actions['jetbot_drive_rz'] = -0.8
        elif detected_focus_size >= 0.05 and 0 < detected_focus_x <= 1.0:
            # buzzer on (if human), lights yellow (if human), turn left
            actions['jetbot_buzzer_status'] = True if detected_human else False
            actions['jetbot_lights_color'] = '#ffff00' if detected_human else '#00ff00'
            actions['jetbot_drive_dx'] = 0.0
            actions['jetbot_drive_rz'] = 0.8
        elif range_distance < 0.5:
            # buzzer off, lights green, turn right
            actions['jetbot_buzzer_status'] = False
            actions['jetbot_lights_color'] = '#00ff00'
            actions['jetbot_drive_dx'] = 0.0
            actions['jetbot_drive_rz'] = -0.8
        else:
            # buzzer off, lights green, drive forward
            actions['jetbot_buzzer_status'] = False
            actions['jetbot_lights_color'] = '#00ff00'
            actions['jetbot_drive_dx'] = 1.0
            actions['jetbot_drive_rz'] = 0.0
        return actions

    def _drive_manual(self):
        actions = dict()

        # check if in a collision state
        if self._state['collision']:
            # buzzer on, lights red
            actions['jetbot_buzzer_status'] = True
            actions['jetbot_lights_color'] = '#ff0000'
            actions['ev3_speec_text'] = 'ouch'
        else:
            # buzzer off, lights blue
            actions['jetbot_lights_color'] = '#0000ff'
            actions['jetbot_buzzer_status'] = False

        """
        NOTE: JOYSTICK INDEX - XBOX (WIRED)
        AXES:       0: LEFT STICK (X) [1.0 to -1.0]     4: LEFT STICK (Y) [-1.0 to 1.0]
                    1: LEFT STICK (Y) [-1.0 to 1.0]     5: RIGHT TRIGGER [1.0 to -1.0]
                    2: LEFT TRIGGER [1.0 to -1.0]       6: BUTTON LEFT/RIGHT [1.0 to -1.0]
                    3: RIGHT STICK (X) [1.0 to -1.0]    7: BUTTON UP/DOWN [-1.0 to 1.0]
        BUTTONS:    0: A    1: B    2: X    3: Y    4: LB   5: RB   6: BACK 7: START
        """
        # fetch most current joystick message
        joy_elapsed = self._sub_listeners['joy_topic'].compute_elapsed()
        joy_msg = self._percepts['joy_topic']
        if joy_elapsed >= self._stale_threshold or not isinstance(joy_msg, Joy):
            # abort if no joystick message
            actions['jetbot_drive_dx'] = 0
            actions['jetbot_drive_rz'] = 0
            return actions

        # determine drive actions
        actions['jetbot_drive_dx'] = joy_msg.axes[7]
        actions['jetbot_drive_rz'] = joy_msg.axes[6]

        # determine cam servo actions
        if joy_msg.buttons[7]:
            actions['jetbot_cam_reset'] = True
            actions['jetbot_cam_rx'] = 0
            actions['jetbot_cam_rz'] = 0
        else:
            actions['jetbot_cam_reset'] = False
            actions['jetbot_cam_rx'] = -1 if joy_msg.axes[4] < -0.67 else 1 if joy_msg.axes[4] > 0.67 else 0
            actions['jetbot_cam_rz'] = -1 if joy_msg.axes[3] > 0.67 else 1 if joy_msg.axes[3] < -0.67 else 0

        # determine arm/hand actions
        actions['ev3_arm_action'] = 'up' if joy_msg.buttons[4] else 'down' if joy_msg.axes[2] < -0.67 else None
        actions['ev3_hand_action'] = 'open' if joy_msg.buttons[5] else 'close' if joy_msg.axes[5] < -0.67 else None

        return actions

    def _request_mode_change(self, req_msg):
        # queue request and return response
        self._request_queue.put(req_msg)
        return AdapaModeChangeServiceResponse(status='')

    def _update_arm(self, actions):
        ev3_arm_action = actions.get('ev3_arm_action')
        if ev3_arm_action is not None:
            # publish arm message
            msg = EV3ArmMessage()
            msg.header.stamp = rospy.Time.now()
            msg.action = ev3_arm_action
            self._pubs['ev3_arm_topic'].publish(msg)

            # update state
            if ev3_arm_action == 'down':
                self._state['ev3_arm'] = 'down'
            elif ev3_arm_action == 'up':
                self._state['ev3_arm'] = 'up'
            else:
                self._state['ev3_arm'] = 'unknown'

    def _update_hand(self, actions):
        ev3_hand_action = actions.get('ev3_hand_action')
        if ev3_hand_action is not None:
            # publish hand message
            msg = EV3HandMessage()
            msg.header.stamp = rospy.Time.now()
            msg.action = ev3_hand_action
            self._pubs['ev3_hand_topic'].publish(msg)

            # update state
            if ev3_hand_action == 'close':
                self._state['ev3_hand'] = 'closed'
            elif ev3_hand_action == 'open':
                self._state['ev3_hand'] = 'opened'
            else:
                self._state['ev3_hand'] = 'unknown'

    def _update_buzzer(self, actions):
        jetbot_buzzer_status = actions.get('jetbot_buzzer_status')
        if jetbot_buzzer_status is not None:
            # publish buzzer message
            msg = JetbotBuzzerMessage()
            msg.header.stamp = rospy.Time.now()
            msg.action = 'on' if jetbot_buzzer_status else 'off'
            self._pubs['jetbot_buzzer_topic'].publish(msg)

            # update state
            self._state['jetbot_buzzer'] = jetbot_buzzer_status

    def _update_drive(self, actions):
        jetbot_drive_dx = actions.get('jetbot_drive_dx')
        jetbot_drive_rz = actions.get('jetbot_drive_rz')
        if jetbot_drive_dx is not None and jetbot_drive_rz is not None:
            # publish drive message
            msg = JetbotDriveMessage()
            msg.header.stamp = rospy.Time.now()
            msg.twist.linear.x = jetbot_drive_dx
            msg.twist.angular.z = jetbot_drive_rz
            self._pubs['jetbot_drive_topic'].publish(msg)

            # update state
            self._state['jetbot_drive_dx'] = jetbot_drive_dx
            self._state['jetbot_drive_rz'] = jetbot_drive_rz

    def _update_lights(self, actions):
        jetbot_lights_color = actions.get('jetbot_lights_color')
        if jetbot_lights_color is not None:
            # publish lights message
            msg = JetbotLightMessage()
            msg.header.stamp = rospy.Time.now()
            msg.color = jetbot_lights_color
            self._pubs['jetbot_lights_topic'].publish(msg)

            # update state
            self._state['jetbot_lights'] = jetbot_lights_color

    def _update_servos(self, actions):
        jetbot_cam_reset = actions.get('jetbot_cam_reset')
        jetbot_cam_rx = actions.get('jetbot_cam_rx')
        jetbot_cam_rz = actions.get('jetbot_cam_rz')
        if jetbot_cam_reset is not None and jetbot_cam_rx is not None and jetbot_cam_rz is not None:
            # publish servos message
            msg = JetbotServosMessage()
            msg.header.stamp = rospy.Time.now()
            msg.cam_reset = jetbot_cam_reset
            msg.cam_rx = jetbot_cam_rx
            msg.cam_rz = jetbot_cam_rz
            self._pubs['jetbot_servos_topic'].publish(msg)

            # update state
            self._state['jetbot_cam_reset'] = jetbot_cam_reset
            self._state['jetbot_cam_rx'] = jetbot_cam_rx
            self._state['jetbot_cam_rz'] = jetbot_cam_rz

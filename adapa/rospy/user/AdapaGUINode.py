from adapa.rospy.detection.AdapaDetectionNode import AdapaDetectionNode
from adapa.rospy.detection.AdapaFaceTrackingNode import AdapaFaceTrackingNode
from adapa.rospy.util.TopicListener import TopicListener
from adapa_ros.msg import AdapaControlMessage
from adapa_ros.msg import AdapaDetectionMessage
from adapa_ros.msg import AdapaFaceTrackingMessage
from adapa_ros.msg import EV3BatteryMessage
from adapa_ros.msg import EV3TouchMessage
from adapa_ros.msg import EV3UltrasonicMessage
from adapa_ros.msg import JetbotBatteryMessage
from adapa_ros.msg import JetbotCameraMessage
from adapa_ros.msg import UserCameraMessage
from anunnaki_torch.adapa_detection import DATA_CLASSES
from anunnaki_torch.adapa_detection import DATA_COLORS
from anunnaki_torch.adapa_detection import DATA_IMAGE_SIZE
from anunnaki_torch.adapa_detection.backend.visualization import annotate_image
from collections import namedtuple
from copy import deepcopy
from datetime import datetime
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from sensor_msgs.msg import Joy
from queue import Queue
import io
import os
import rospy
import time
import sys


class AdapaGUINode(object):
    def __init__(
            self,
            name='adapa_gui',
            hud_display_size=(480, 480),
            user_display_size=(160, 120),
            capture_dir=None,
            stale_threshold=1.0,
            refresh_rate=30,
    ):
        self._hud_display_size = hud_display_size
        self._user_display_size = user_display_size
        self._capture_dir = os.path.expanduser(capture_dir) if capture_dir is not None else '.'
        self._capture_time = time.time()
        self._stale_threshold = stale_threshold
        self._refresh_rate = refresh_rate

        # initialize node and topic
        rospy.init_node(name)
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # define topic fields (for display)
        self._topic_index = dict(
            adapa_control_topic=[
                'collision',
                'detected_deer',
                'detected_human',
                'detected_focus_size',
                'detected_focus_x',
                'detected_focus_y',
                'detection_stale',
                'ev3_arm',
                'ev3_battery_voltage',
                'ev3_battery_status',
                'ev3_hand',
                'ev3_ultrasonic_range',
                'ev3_ultrasonic_range_max',
                'ev3_ultrasonic_stale',
                'jetbot_battery_voltage',
                'jetbot_battery_status',
                'jetbot_buzzer',
                'jetbot_cam_reset',
                'jetbot_cam_rx',
                'jetbot_cam_rz',
                'jetbot_drive_dx',
                'jetbot_drive_rz',
                'jetbot_lights',
                'mode',
                'n_collisions',
            ],
            adapa_detection_topic=None,
            adapa_facetracking_topic=None,
            jetbot_battery_topic=[
                'status',
                'voltage',
            ],
            ev3_battery_topic=[
                'status',
                'current',
                'voltage',
            ],
            ev3_touch_topic=[
                'touched',
            ],
            ev3_ultrasonic_topic=[
                'range.range',
            ],
        )

        # initialize subscribers
        self._sub_thread = None
        self._sub_listeners = dict(
            adapa_control_topic=TopicListener('/adapa/control/state', AdapaControlMessage),
            adapa_detection_topic=TopicListener('/adapa/sensor/detection', AdapaDetectionMessage),
            adapa_facetracking_topic=TopicListener('/adapa/sensor/facetracking', AdapaFaceTrackingMessage),
            ev3_battery_topic=TopicListener('/ev3/sensor/battery', EV3BatteryMessage),
            ev3_touch_topic=TopicListener('/ev3/sensor/touch', EV3TouchMessage),
            ev3_ultrasonic_topic=TopicListener('/ev3/sensor/ultrasonic', EV3UltrasonicMessage),
            jetbot_battery_topic=TopicListener('/jetbot/sensor/battery', JetbotBatteryMessage),
            jetbot_camera_topic=TopicListener('/jetbot/sensor/camera', JetbotCameraMessage),
            joy_topic=TopicListener('/joy', Joy),
            user_camera_topic=TopicListener('/user/sensor/camera', UserCameraMessage),
        )
        self._sub_updates = {key: None for key, manager in self._sub_listeners.items()}
        self._sub_update_queue = Queue()

        # create qt app and main window
        font = QtGui.QFont()
        font.setWeight(12)
        self._app = QtWidgets.QApplication([])
        self._win = QtWidgets.QMainWindow()
        self._win.setWindowTitle('Adapa')
        self._win.setFont(font)

        # create central root widget
        self._root = QtWidgets.QWidget()
        self._win.setCentralWidget(self._root)

        # hud widgets
        self._hud_frame = QtWidgets.QFrame(parent=self._root)
        self._hud_canvas_frame = QtWidgets.QFrame(parent=self._hud_frame)
        self._hud_canvas_frame.setFixedHeight(self._hud_display_size[1])
        self._hud_canvas_frame.setStyleSheet('background: #222222;')
        self._hud_canvas = QtWidgets.QLabel(parent=self._hud_canvas_frame)
        self._hud_canvas.setFixedSize(self._hud_display_size[0], self._hud_display_size[1])
        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._hud_canvas)
        self._hud_canvas_frame.setLayout(layout)
        self._hud_checkbox_frame = QtWidgets.QFrame(parent=self._hud_frame)
        self._hud_checkbox = QtWidgets.QCheckBox('HUD', parent=self._hud_checkbox_frame)
        self._hud_checkbox.setChecked(True)
        self._detection_checkbox = QtWidgets.QCheckBox('Detections', parent=self._hud_checkbox_frame)
        self._detection_checkbox.setChecked(True)
        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._hud_checkbox)
        layout.addWidget(self._detection_checkbox)
        layout.addStretch()
        self._hud_checkbox_frame.setLayout(layout)
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._hud_canvas_frame)
        layout.addWidget(self._hud_checkbox_frame)
        layout.addStretch()
        self._hud_frame.setLayout(layout)

        self._user_frame = QtWidgets.QFrame(parent=self._root)
        self._user_canvas_frame = QtWidgets.QFrame(parent=self._user_frame)
        self._user_canvas_frame.setFixedHeight(self._user_display_size[1])
        self._user_canvas_frame.setStyleSheet('background: #222222;')
        self._user_canvas = QtWidgets.QLabel(parent=self._user_canvas_frame)
        self._user_canvas.setFixedSize(self._user_display_size[0], self._user_display_size[1])
        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._user_canvas)
        self._user_canvas_frame.setLayout(layout)
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._user_canvas_frame)
        layout.addStretch()
        self._user_frame.setLayout(layout)

        # create topics frame
        self._topics_frame = QtWidgets.QFrame(parent=self._root)
        self._topics_inner_frame = QtWidgets.QFrame(parent=self._topics_frame)
        layout = QtWidgets.QVBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignTop)
        layout.setContentsMargins(3, 3, 3, 3)
        self._topic_widgets = {
            key: TopicWidget(self._sub_listeners[key].topic, fields, parent=self._topics_inner_frame)
            for key, fields in self._topic_index.items()
        }
        for key, widget in self._topic_widgets.items():
            layout.addWidget(widget)
        layout.addStretch()
        self._topics_inner_frame.setLayout(layout)
        self._topics_scroll = QtWidgets.QScrollArea(parent=self._topics_frame)
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self._topics_inner_frame)
        layout.addStretch()
        self._topics_scroll.setLayout(layout)
        self._topics_scroll.setWidget(self._topics_inner_frame)
        self._topics_scroll.setWidgetResizable(True)
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._topics_scroll)
        self._topics_frame.setLayout(layout)
        self._topics_frame.setFixedWidth(350)

        self._right_frame = QtWidgets.QFrame(self._root)
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._user_frame)
        layout.addWidget(self._topics_frame)
        self._right_frame.setLayout(layout)

        # layout root widget
        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._hud_frame)
        layout.addWidget(self._right_frame)
        self._root.setLayout(layout)

        # init idle timer
        self._timer = QtCore.QTimer(self._root)
        self._timer.setInterval(int(1000 / self._refresh_rate))

    def idle(self):
        self._refresh()
        if self._sub_thread and self._sub_thread.isRunning():
            if not self._timer.isActive():
                self._timer.timeout.connect(self.idle)
                self._timer.start()
        else:
            self._timer.timeout.disconnect()
            self._timer.stop()

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))

        # begin execution in background thread
        if self._sub_thread is None:
            self._sub_thread = SubscriberThread(self._sub_listeners, refresh_rate=self._refresh_rate)
            self._sub_thread.on_update.connect(self._on_sub_update)
            self._sub_thread.start()

        # run idle loop
        self.idle()

        # show and execute main window
        self._win.show()
        exit_code = self._app.exec_()
        self.terminate()
        sys.exit(exit_code)

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        if self._sub_thread is not None and self._sub_thread.isRunning():
            self._sub_thread.quit()
            # self._sub_thread.wait(1000)

    def _on_sub_update(self, event):
        self._sub_update_queue.put(event.updates)

    def _refresh(self):
        # get most recent updates
        while self._sub_update_queue.qsize() > 0:
            self._sub_updates = self._sub_update_queue.get(block=False)

        self._refresh_display()
        self._refresh_topics()
        self._refresh_capture()

    def _refresh_capture(self):
        # get current camera and joystick data
        joy_elapsed = self._sub_listeners['joy_topic'].compute_elapsed()
        joy_msg = self._sub_updates.get('joy_topic')
        if joy_elapsed < self._stale_threshold and isinstance(joy_msg, Joy):
            # determine category
            cat = None

            if joy_msg.buttons[0]:
                cat = 1
            elif joy_msg.buttons[1]:
                cat = 2
            elif joy_msg.buttons[2]:
                cat = 3
            elif joy_msg.buttons[3]:
                cat = 4

            elapsed = time.time() - self._capture_time
            if cat is not None and elapsed > 0.1:
                # set file path and directory
                timestamp = datetime.now().strftime('%Y%m%d_%H%M%S%f')
                file_path = os.path.join(self._capture_dir, 'cat{}'.format(cat), '{}.jpg'.format(timestamp))
                file_dir = os.path.dirname(file_path)
                if not os.path.exists(file_dir):
                    os.makedirs(file_dir)

                # save camera image
                camera_elapsed = self._sub_listeners['jetbot_camera_topic'].compute_elapsed()
                camera_msg = self._sub_updates.get('jetbot_camera_topic')
                if camera_elapsed < self._stale_threshold and isinstance(camera_msg, JetbotCameraMessage):
                    img = Image.open(io.BytesIO(camera_msg.image.data))
                    img.save(file_path)
                    rospy.loginfo('Saved image to "{}".'.format(file_path))
                    self._capture_time = time.time()

        # erase joystick data from memory
        self._sub_updates['joy_topic'] = None

    def _refresh_display(self):
        self._refresh_display_hud()
        self._refresh_display_user()

    def _refresh_display_hud(self):
        camera_elapsed = self._sub_listeners['jetbot_camera_topic'].compute_elapsed()
        camera_msg = self._sub_updates.get('jetbot_camera_topic')
        if not isinstance(camera_msg, JetbotCameraMessage):
            # draw null image
            text = '[no data]'
            img = Image.new('RGB', self._hud_display_size, color=0)
            draw = ImageDraw.Draw(img)
            text_width, text_height = draw.textsize(text)
            text_xy = ((img.width - text_width) / 2, (img.height - text_height) / 2)
            draw.text(text_xy, text, fill='white')
        else:
            # get image from camera
            img = Image.open(io.BytesIO(camera_msg.image.data))
            if camera_elapsed >= self._stale_threshold:
                img = img.convert('L').convert('RGB')

            # draw detections on image
            if self._hud_checkbox.isChecked() and self._detection_checkbox.isChecked():
                detection_elapsed = self._sub_listeners['adapa_detection_topic'].compute_elapsed()
                detection_msg = self._sub_updates['adapa_detection_topic']
                if detection_elapsed < self._stale_threshold and isinstance(detection_msg, AdapaDetectionMessage):
                    detections = AdapaDetectionNode.message_to_detections(detection_msg)
                    img = _draw_hud_detections(img, detections)

            # draw detection reticle
            control_msg = self._sub_updates['adapa_control_topic']
            if control_msg and self._hud_checkbox.isChecked():
                if isinstance(control_msg, AdapaControlMessage):
                    if control_msg.detected_human:
                        detection_xy = (control_msg.detected_focus_x, control_msg.detected_focus_y)
                        _draw_hud_reticle(img, detection_xy)

            # draw hud
            control_msg = self._sub_updates['adapa_control_topic']
            if control_msg and self._hud_checkbox.isChecked():
                lights_color = control_msg.jetbot_lights

                # determine background color
                background_color = '#00000066'
                if lights_color.startswith('#'):
                    background_color = '{}66'.format(lights_color)

                # determine detection caption
                detection_caption = None
                if control_msg.detected_human:
                    detection_caption = 'human'
                elif control_msg.detected_deer:
                    detection_caption = 'deer'

                # determine ev3 battery color
                ev3_battery_color = '#ffffff66'
                if control_msg.ev3_battery_status == 'high':
                    ev3_battery_color = '#00ff0066'
                elif control_msg.ev3_battery_status == 'medium':
                    ev3_battery_color = '#00880066'
                elif control_msg.ev3_battery_status == 'low':
                    ev3_battery_color = '#ff000066'

                # determine jetbot battery color
                jetbot_battery_color = '#ffffff66'
                if control_msg.jetbot_battery_status == 'high':
                    jetbot_battery_color = '#00ff0066'
                elif control_msg.jetbot_battery_status == 'medium':
                    jetbot_battery_color = '#00880066'
                elif control_msg.jetbot_battery_status == 'low':
                    jetbot_battery_color = '#ff000066'

                img = _draw_hud_info(
                    img,
                    control_msg.ev3_battery_voltage,
                    control_msg.ev3_ultrasonic_range,
                    control_msg.jetbot_battery_voltage,
                    control_msg.jetbot_buzzer,
                    background_color=background_color,
                    detection_caption=detection_caption,
                    ev3_battery_color=ev3_battery_color,
                    jetbot_battery_color=jetbot_battery_color,
                )

        # save camera image to bytes and load into pixmap
        img_bytes = io.BytesIO()
        img = img.resize(self._hud_display_size, resample=Image.NEAREST)
        img.save(img_bytes, format='JPEG')
        img = QtGui.QImage()
        img.loadFromData(img_bytes.getvalue())
        self._hud_canvas.setPixmap(QtGui.QPixmap.fromImage(img))

    def _refresh_display_user(self):
        # get user camera image
        camera_elapsed = self._sub_listeners['user_camera_topic'].compute_elapsed()
        camera_msg = self._sub_updates.get('user_camera_topic')
        if not isinstance(camera_msg, UserCameraMessage):
            self._user_frame.hide()
        else:
            self._user_frame.show()

            # get image from camera
            img = Image.open(io.BytesIO(camera_msg.image.data))
            if camera_elapsed >= self._stale_threshold:
                img = img.convert('L').convert('RGB')

            # draw face tracking on image
            tracking_elapsed = self._sub_listeners['adapa_facetracking_topic'].compute_elapsed()
            tracking_msg = self._sub_updates['adapa_facetracking_topic']
            if tracking_elapsed < self._stale_threshold and isinstance(tracking_msg, AdapaFaceTrackingMessage):
                face_landmarks, euler_angles, axis_points = AdapaFaceTrackingNode.message_to_detections(tracking_msg)
                img = _draw_eye_tracking(img, face_landmarks, euler_angles, axis_points)

            # save camera image to bytes and load into pixmap
            img_bytes = io.BytesIO()
            img = img.resize(self._user_display_size, resample=Image.NEAREST)
            img.save(img_bytes, format='JPEG')
            img = QtGui.QImage()
            img.loadFromData(img_bytes.getvalue())
            self._user_canvas.setPixmap(QtGui.QPixmap.fromImage(img))

    def _refresh_topics(self):
        # update field values in topic widgets
        for key, widget in self._topic_widgets.items():
            if key in self._topic_index:
                msg = self._sub_updates.get(key)
                msg_rate = 0
                msg_elapsed = 0
                if key in self._sub_listeners:
                    msg_rate = self._sub_listeners[key].compute_rate()
                    msg_elapsed = self._sub_listeners[key].compute_elapsed()
                widget.update_values(msg, msg_rate=msg_rate, msg_elapsed=msg_elapsed, stale_threshold=self._stale_threshold)


class SubscriberThread(QtCore.QThread):
    UpdateEvent = namedtuple('UpdateEvent', ['updates'])
    on_update = QtCore.pyqtSignal(object)

    def __init__(self, sub_listeners, refresh_rate=30):
        super().__init__()
        self._sub_listeners = sub_listeners
        self._refresh_rate = refresh_rate

        # init updates
        self._sub_updates = {key: None for key, manager in self._sub_listeners.items()}

        # init subscribers
        self._subs = {
            key: rospy.Subscriber(manager.topic, manager.data_class, callback=manager.callback)
            for key, manager in self._sub_listeners.items()
        }

    def run(self):
        try:
            rate = rospy.Rate(self._refresh_rate)
            while self.isRunning() and not rospy.is_shutdown():
                # get most current messages from subscriptions and update with newest messages
                for key, manager in self._sub_listeners.items():
                    msg = manager.fetch_newest_message()
                    if key in self._sub_updates and msg is not None:
                        self._sub_updates[key] = msg

                # fire update event
                try:
                    self.on_update.emit(self.UpdateEvent(deepcopy(self._sub_updates)))
                except AttributeError:
                    pass

                # sleep until next iteration
                rate.sleep()
        except rospy.ROSInterruptException:
            pass

        # unregister subscribers
        for topic, sub in self._subs.items():
            sub.unregister()


class TopicWidget(QtWidgets.QFrame):
    def __init__(self, topic, fields, *args, parent=None, **kwargs):
        super().__init__(*args, parent=parent, **kwargs)
        self._topic = topic
        self._fields = {field: dict() for field in fields} if fields is not None else dict()

        self._frame_style = 'QFrame { background-color: #ffffff; border: 1px solid #000000; }'
        self._label_style_stale = 'QLabel { color: #ffffff; background-color: #888888; border: none; padding: 3px;}'
        self._label_style_valid = 'QLabel { color: #ffffff; background-color: #0f754e; border: none; padding: 3px;}'
        self._value_style = 'QLabel { background-color: #ffffff; border: none; font-style: italic; padding: 3px;}'

        # create widgets
        self._topic_label = QtWidgets.QLabel(parent=self)
        self._topic_label.setStyleSheet('QLabel { font-style: italic; font-weight: bold; }')
        self._topic_label.setText('[{}]'.format(self._topic))
        fields_frame = QtWidgets.QFrame(parent=self)
        for field in self._fields:
            field_frame = QtWidgets.QFrame(parent=self)
            field_frame.setAttribute(QtCore.Qt.WA_StyledBackground, True)
            field_frame.setFixedWidth(300)
            field_frame.setStyleSheet(self._frame_style)

            label_widget = QtWidgets.QLabel(parent=field_frame)
            label_widget.setAlignment(QtCore.Qt.AlignLeft)
            label_widget.setAttribute(QtCore.Qt.WA_StyledBackground, True)
            label_widget.setFixedWidth(195)
            label_widget.setStyleSheet(self._label_style_stale)
            label_widget.setText('{}'.format(field.split('.')[-1]))

            value_widget = QtWidgets.QLabel(parent=field_frame)
            value_widget.setAlignment(QtCore.Qt.AlignHCenter)
            value_widget.setAttribute(QtCore.Qt.WA_StyledBackground, True)
            value_widget.setFixedWidth(95)
            value_widget.setStyleSheet(self._value_style)
            value_widget.setText('')

            layout = QtWidgets.QHBoxLayout()
            layout.setContentsMargins(1, 1, 1, 1)
            layout.addWidget(label_widget)
            layout.addWidget(value_widget)
            layout.addStretch()
            field_frame.setLayout(layout)
            self._fields[field] = dict(frame=field_frame, label=label_widget, value=value_widget)

        # set fields layout
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        for field in self._fields:
            layout.addWidget(self._fields[field]['frame'])
        fields_frame.setLayout(layout)

        # set layout
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._topic_label)
        layout.addWidget(fields_frame)
        layout.addStretch()
        self.setLayout(layout)

        # set attributes
        self.setMinimumWidth(300)

    def update_values(self, msg, msg_rate=0.0, msg_elapsed=0.0, stale_threshold=1.0):
        if isinstance(msg_rate, (int, float)):
            self._topic_label.setText('[{}] ({:d} Hz)'.format(self._topic, int(msg_rate)))

        if msg is None:
            for field in self._fields:
                self._fields[field]['label'].setStyleSheet(self._label_style_stale)
                self._fields[field]['value'].setText('[no data]')
        else:
            style = self._label_style_valid if msg_elapsed < stale_threshold else self._label_style_stale
            for field in self._fields:
                field_value = msg
                for field_attr in field.split('.'):
                    field_value = getattr(field_value, field_attr)
                if isinstance(field_value, float):
                    text = '{:.2f}'.format(field_value)
                else:
                    text = '{}'.format(field_value)
                self._fields[field]['label'].setStyleSheet(style)
                self._fields[field]['value'].setText(text)


def _draw_eye_tracking(image, face_landmarks, euler_angles, axis_points, font='verdana.ttf'):
    # prepare image for drawing
    draw = ImageDraw.Draw(image, mode='RGBA')

    # initialize image font
    try:
        font = ImageFont.truetype(font, 12)
    except OSError:
        font = None

    # draw face landmarks
    gaze_caption = 'UNKNOWN'
    if face_landmarks.sum() > 0:
        # draw landmark markers
        for x, y in face_landmarks:
            x = int(x * image.width)
            y = int(y * image.height)
            draw.ellipse((x, y, x + 3, y + 3), fill='#ffff0044', outline='#00000000')

        # draw pose coord axes
        draw.line([tuple(axis_points[3].ravel()), tuple(axis_points[0].ravel())], fill='#ff0000aa', width=3)
        draw.line([tuple(axis_points[3].ravel()), tuple(axis_points[1].ravel())], fill='#00ff00aa', width=3)
        draw.line([tuple(axis_points[3].ravel()), tuple(axis_points[2].ravel())], fill='#0000ffaa', width=3)

        # determine direction of gaze from pitch
        gaze_caption = 'FORWARD'
        if euler_angles[1] < -33:
            gaze_caption = 'LEFT'
        elif euler_angles[1] > 33:
            gaze_caption = 'RIGHT'

    # display text
    text_w, text_h = font.getsize(gaze_caption) if font is not None else (7 * len(gaze_caption), 10)
    text_box_x, text_box_y = ((image.width - text_w) / 2, 0)
    text_box_w = text_box_x + text_w + 4
    text_box_h = text_box_y + text_h + 4
    draw.rectangle((text_box_x, text_box_y, text_box_w, text_box_h), fill='#00000088')
    draw.text((text_box_x + 2, text_box_y + 2), gaze_caption, fill='#ffffffff', font=font)

    return image

def draw_axis(img, R, t, K):
    # unit is mm
    rotV, _ = cv2.Rodrigues(R)
    points = np.float32([[100, 0, 0], [0, 100, 0], [0, 0, 100], [0, 0, 0]]).reshape(-1, 3)
    axisPoints, _ = cv2.projectPoints(points, rotV, t, K, (0, 0, 0, 0))
    img = cv2.line(img, tuple(axisPoints[3].ravel()), tuple(axisPoints[0].ravel()), (255,0,0), 3)
    img = cv2.line(img, tuple(axisPoints[3].ravel()), tuple(axisPoints[1].ravel()), (0,255,0), 3)
    img = cv2.line(img, tuple(axisPoints[3].ravel()), tuple(axisPoints[2].ravel()), (0,0,255), 3)
    return img


def _draw_hud_detections(image, detections):
    image = annotate_image(
        image,
        detections,
        caption_mapping=DATA_CLASSES,
        color_mapping=DATA_COLORS,
        scaling=image.size,
    )
    return image


def _draw_hud_info(
        image,
        ev3_battery_voltage,
        ev3_ultrasonic_range,
        jetbot_battery_voltage,
        jetbot_buzzer,
        background_color='#00000066',
        detection_caption=None,
        ev3_battery_color='#ffffff66',
        jetbot_battery_color='#ffffff66',
):
    # draw hud box
    draw = ImageDraw.Draw(image, mode='RGBA')
    _, hud_height = draw.textsize('HUD')
    hud_box = (0, 0, image.width, hud_height + 8)
    hud_offset = 5
    draw.rectangle(hud_box, fill=background_color, outline='#00000066')

    # draw buzzer text
    buzzer_text = '(!)' if jetbot_buzzer else ''
    buzzer_text = buzzer_text.ljust(3)
    buzzer_text_width, buzzer_text_height = draw.textsize(buzzer_text)
    buzzer_text_xy = (5, hud_offset)
    draw.text(buzzer_text_xy, buzzer_text, fill='white')

    # draw range text
    range_text = '{:.2f} m'.format(ev3_ultrasonic_range) if ev3_ultrasonic_range is not None else ''
    range_text = range_text.ljust(6)
    range_text_width, range_text_height = draw.textsize(range_text)
    range_text_xy = (buzzer_text_xy[0] + buzzer_text_width + 5, hud_offset)
    draw.text(range_text_xy, range_text, fill='white')

    # draw detection text
    detection_text = '{}'.format(detection_caption) if detection_caption is not None else ''
    detection_text = detection_text.ljust(5)
    detection_text_width, detection_text_height = draw.textsize(detection_text)
    detection_text_xy = (range_text_xy[0] + range_text_width + 5, hud_offset)
    draw.text(detection_text_xy, detection_text, fill='white')

    # draw battery text
    if jetbot_battery_voltage is not None:
        jetbot_battery_text = '{:.1f} V'.format(jetbot_battery_voltage)
        jetbot_battery_text = jetbot_battery_text.ljust(6)
        jetbot_battery_text_width, jetbot_battery_text_height = draw.textsize(jetbot_battery_text)
        jetbot_battery_text_xy = (image.width - jetbot_battery_text_width - 10, hud_offset)
        jetbot_battery_box = (
            jetbot_battery_text_xy[0] - 4,
            jetbot_battery_text_xy[1] - 3,
            jetbot_battery_text_xy[0] + jetbot_battery_text_width + 2,
            jetbot_battery_text_xy[1] + jetbot_battery_text_height + 1,
        )
        jetbot_battery_head_box = (
            jetbot_battery_text_xy[0] + jetbot_battery_text_width + 2,
            jetbot_battery_text_xy[1],
            jetbot_battery_text_xy[0] + jetbot_battery_text_width + 6,
            jetbot_battery_text_xy[1] + jetbot_battery_text_height - 2,
        )
        draw.rounded_rectangle(jetbot_battery_box, fill=jetbot_battery_color, outline='#00000066', radius=1)
        draw.rectangle(jetbot_battery_head_box, fill=jetbot_battery_color, outline='#00000066')
        draw.text(jetbot_battery_text_xy, jetbot_battery_text, fill='white')

    if ev3_battery_voltage is not None:
        ev3_battery_text = ' {:.1f} V'.format(ev3_battery_voltage)
        ev3_battery_text = ev3_battery_text.ljust(6)
        ev3_battery_text_width, ev3_battery_text_height = draw.textsize(ev3_battery_text)
        ev3_battery_text_xy = (image.width - ev3_battery_text_width * 2.75, hud_offset)
        ev3_battery_box = (
            ev3_battery_text_xy[0] - 4,
            ev3_battery_text_xy[1] - 3,
            ev3_battery_text_xy[0] + ev3_battery_text_width + 2,
            ev3_battery_text_xy[1] + ev3_battery_text_height + 1,
        )
        ev3_battery_head_box = (
            ev3_battery_text_xy[0] + ev3_battery_text_width + 2,
            ev3_battery_text_xy[1],
            ev3_battery_text_xy[0] + ev3_battery_text_width + 6,
            ev3_battery_text_xy[1] + ev3_battery_text_height - 2,
        )
        draw.rounded_rectangle(ev3_battery_box, fill=ev3_battery_color, outline='#00000066', radius=1)
        draw.rectangle(ev3_battery_head_box, fill=ev3_battery_color, outline='#00000066')
        draw.text(ev3_battery_text_xy, ev3_battery_text, fill='white')
    return image


def _draw_hud_reticle(image, detection_xy):
    if detection_xy is not None:
        reticle_color = '#ffffffff'
        reticle_x = detection_xy[0] * image.width
        reticle_y = detection_xy[1] * image.height
        draw = ImageDraw.Draw(image, mode='RGBA')
        draw.line((reticle_x, reticle_y - 6, reticle_x, reticle_y + 6), fill='#00000066', width=3)
        draw.line((reticle_x - 6, reticle_y, reticle_x + 6, reticle_y), fill='#00000066', width=3)
        draw.ellipse((reticle_x - 6, reticle_y - 6, reticle_x + 6, reticle_y + 6), fill=None, outline='#00000066', width=3)
        draw.line((reticle_x, reticle_y - 5, reticle_x, reticle_y + 5), fill=reticle_color, width=1)
        draw.line((reticle_x - 5, reticle_y, reticle_x + 5, reticle_y), fill=reticle_color, width=1)
        draw.ellipse((reticle_x - 5, reticle_y - 5, reticle_x + 5, reticle_y + 5), fill=None, outline=reticle_color, width=1)
    return image

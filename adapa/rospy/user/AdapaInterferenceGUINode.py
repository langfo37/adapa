from adapa_ros.msg import JetbotCameraMessage
from adapa_ros.srv import AdapaInterferenceUpdateService
from adapa.rospy.util.TopicListener import TopicListener
from anunnaki.core.transformation.image import list_transforms
from anunnaki.core.transformation.image import load_transform
from collections import namedtuple
from copy import deepcopy
from sensor_msgs.msg import CompressedImage
from PIL import Image
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from queue import Queue
import io
import json
import rospy
import sys


class AdapaInterferenceGUINode(object):
    def __init__(
            self,
            name='adapa_interference_gui',
            display_size=(480, 480),
            refresh_rate=30,
    ):
        self._display_size = display_size
        self._refresh_rate = refresh_rate
        self._pointer_x = 0.5
        self._pointer_y = 0.5

        # initialize node and topic
        rospy.init_node(name)
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # initialize subscribers
        self._sub_thread = None
        self._sub_listeners = dict(
            jetbot_camera_topic=TopicListener('/jetbot/sensor/camera', JetbotCameraMessage),
        )
        self._sub_updates = {key: None for key, manager in self._sub_listeners.items()}
        self._sub_update_queue = Queue()

        # initialize service proxies
        self._interference_update_proxy = rospy.ServiceProxy('/adapa/interference/update', AdapaInterferenceUpdateService)

        # create qt app and main window
        font = QtGui.QFont()
        font.setWeight(12)
        self._app = QtWidgets.QApplication([])
        self._win = QtWidgets.QMainWindow()
        self._win.setWindowTitle('Adapa Interference')
        self._win.setFont(font)

        # create central root widget
        self._root = QtWidgets.QWidget()
        self._win.setCentralWidget(self._root)

        # display widgets
        self._display_frame = QtWidgets.QFrame(parent=self._root)
        self._display_canvas_frame = QtWidgets.QFrame(parent=self._display_frame)
        self._display_canvas_frame.setFixedHeight(self._display_size[1])
        self._display_canvas_frame.setStyleSheet('background: #222222;')
        self._display_canvas = QtWidgets.QLabel(parent=self._display_canvas_frame)
        self._display_canvas.setFixedSize(self._display_size[0], self._display_size[1])
        clickable(self._display_canvas).connect(self._on_click_canvas)
        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._display_canvas)
        self._display_canvas_frame.setLayout(layout)
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._display_canvas_frame)
        layout.addStretch()
        self._display_frame.setLayout(layout)

        # create transform controls
        self._transform_factory = None
        self._transform_frame = QtWidgets.QFrame(parent=self._root)
        self._transform_selection_frame = QtWidgets.QFrame(parent=self._transform_frame)
        self._transform_selection_label = QtWidgets.QLabel(parent=self._transform_selection_frame)
        self._transform_selection_label.setText('Transform')
        self._transform_selection_label.setMaximumWidth(100)
        self._transform_selection_box = QtWidgets.QComboBox(parent=self._transform_selection_frame)
        self._transform_selection_box.addItems(['NO TRANSFORM'] + list_transforms())
        self._transform_selection_box.currentIndexChanged.connect(self._on_selection_transform)
        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self._transform_selection_label)
        layout.addWidget(self._transform_selection_box)
        self._transform_selection_frame.setLayout(layout)
        self._transform_inner_frame = QtWidgets.QFrame(parent=self._transform_frame)
        self._transform_slider_frames = dict()
        self._transform_slider_titles = dict()
        self._transform_sliders = dict()
        self._transform_slider_values = dict()
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self._transform_inner_frame.setLayout(layout)
        self._populate_transform_sliders()
        self._transform_scroll = QtWidgets.QScrollArea(parent=self._transform_frame)
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self._transform_inner_frame)
        layout.addStretch()
        self._transform_scroll.setLayout(layout)
        self._transform_scroll.setWidget(self._transform_inner_frame)
        self._transform_scroll.setWidgetResizable(True)
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._transform_selection_frame)
        layout.addWidget(self._transform_scroll)
        self._transform_frame.setLayout(layout)
        self._transform_frame.setMinimumWidth(300)

        # layout root widget
        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(self._display_frame)
        layout.addWidget(self._transform_frame)
        self._root.setLayout(layout)

        # init idle timer
        self._timer = QtCore.QTimer(self._root)
        self._timer.setInterval(int(1000 / self._refresh_rate))

    def idle(self):
        self._refresh()
        if self._sub_thread and self._sub_thread.isRunning():
            if not self._timer.isActive():
                self._timer.timeout.connect(self.idle)
                self._timer.start()
        else:
            self._timer.timeout.disconnect()
            self._timer.stop()

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))

        # begin execution in background thread
        if self._sub_thread is None:
            self._sub_thread = SubscriberThread(self._sub_listeners, refresh_rate=self._refresh_rate)
            self._sub_thread.on_update.connect(self._on_sub_update)
            self._sub_thread.start()

        # run idle loop
        self.idle()

        # show and execute main window
        self._win.show()
        exit_code = self._app.exec_()
        self.terminate()
        sys.exit(exit_code)

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        if self._sub_thread is not None and self._sub_thread.isRunning():
            self._sub_thread.quit()
            # self._sub_thread.wait(1000)

    def _on_click_canvas(self, evt):
        x = evt.pos().x()
        y = evt.pos().y()
        self._pointer_x = x / self._display_size[0]
        self._pointer_y = y / self._display_size[1]
        self._request_transform()

    def _on_selection_transform(self, index):
        transform_type = self._transform_selection_box.currentText().lower()
        self._transform_factory = load_transform(transform_type)
        self._populate_transform_sliders()
        self._request_transform()

    def _on_slider_transform(self):
        # update text for each transform slider
        if self._transform_factory is not None and hasattr(self._transform_factory, 'SPECIFICATION'):
            transform_specification = self._transform_factory.SPECIFICATION
            for k in self._transform_sliders:
                slider_caption = transform_slider_caption(transform_specification[k], self._transform_sliders[k].value())
                self._transform_slider_values[k].setText(slider_caption)

        self._request_transform()

    def _on_sub_update(self, event):
        self._sub_update_queue.put(event.updates)

    def _populate_transform_sliders(self):
        # remove existing widgets
        for i in reversed(range(self._transform_inner_frame.layout().count())):
            widget = self._transform_inner_frame.layout().itemAt(i).widget()
            self._transform_inner_frame.layout().removeWidget(widget)
        for k, widget in self._transform_slider_frames.items():
            widget.deleteLater()
        for k, widget in self._transform_slider_titles.items():
            widget.deleteLater()
        for k, widget in self._transform_sliders.items():
            widget.deleteLater()
        for k, widget in self._transform_slider_values.items():
            widget.deleteLater()
        self._transform_slider_frames = dict()
        self._transform_slider_titles = dict()
        self._transform_sliders = dict()
        self._transform_slider_values = dict()

        # check if transform factory is valid
        if self._transform_factory is None:
            return
        if not hasattr(self._transform_factory, 'SPECIFICATION'):
            return
        if not hasattr(self._transform_factory, 'DEFAULTS'):
            return

        # create widgets for each transform parameter in specification
        transform_defaults = deepcopy(self._transform_factory.DEFAULTS)
        transform_specification = deepcopy(self._transform_factory.SPECIFICATION)
        self._canvas_vars = sorted([k for k in transform_specification if k.endswith('_x') or k.endswith('_y')])
        for k in transform_specification:
            if k in self._canvas_vars:
                continue
            self._transform_slider_frames[k] = QtWidgets.QFrame(parent=self._transform_inner_frame)
            self._transform_slider_titles[k] = QtWidgets.QLabel(parent=self._transform_slider_frames[k])
            self._transform_slider_titles[k].setText(k)
            self._transform_sliders[k] = QtWidgets.QSlider(QtCore.Qt.Horizontal, parent=self._transform_slider_frames[k])
            self._transform_sliders[k].setFocusPolicy(QtCore.Qt.StrongFocus)
            self._transform_sliders[k].setMaximum(100)
            self._transform_sliders[k].setMinimum(0)
            self._transform_sliders[k].setSingleStep(1)
            self._transform_sliders[k].setTickInterval(5)
            self._transform_sliders[k].setTickPosition(QtWidgets.QSlider.NoTicks)
            slider_index = transform_slider_index(transform_specification[k], transform_defaults[k])
            self._transform_sliders[k].setValue(slider_index)
            self._transform_sliders[k].valueChanged.connect(self._on_slider_transform)
            self._transform_slider_values[k] = QtWidgets.QLabel(parent=self._transform_slider_frames[k])
            slider_caption = transform_slider_caption(transform_specification[k], self._transform_sliders[k].value())
            self._transform_slider_values[k].setText(slider_caption)
            layout = QtWidgets.QHBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            layout.addWidget(self._transform_slider_titles[k])
            layout.addWidget(self._transform_sliders[k])
            layout.addWidget(self._transform_slider_values[k])
            self._transform_slider_frames[k].setLayout(layout)
            self._transform_inner_frame.layout().addWidget(self._transform_slider_frames[k])
        self._transform_inner_frame.layout().addStretch()

    def _refresh(self):
        # get most recent updates
        while self._sub_update_queue.qsize() > 0:
            self._sub_updates = self._sub_update_queue.get(block=False)

        # get image from message
        image = None
        image_msg = self._sub_updates.get('jetbot_camera_topic')
        if hasattr(image_msg, 'image'):
            image_msg = image_msg.image
        if isinstance(image_msg, CompressedImage):
            image = Image.open(io.BytesIO(image_msg.data))

        # update displayed image
        if isinstance(image, Image.Image):
            # save camera image to bytes and load into pixmap
            image_bytes = io.BytesIO()
            image = image.resize(self._display_size, resample=Image.NEAREST)
            image.save(image_bytes, format='JPEG')
            image = QtGui.QImage()
            image.loadFromData(image_bytes.getvalue())
            self._display_canvas.setPixmap(QtGui.QPixmap.fromImage(image))

    def _request_transform(self):
        transform_type = self._transform_selection_box.currentText().lower()
        transform_context = dict()
        if self._transform_factory is not None and hasattr(self._transform_factory, 'SPECIFICATION'):
            # determine transform context
            transform_specification = self._transform_factory.SPECIFICATION
            transform_context = {
                k: transform_slider_value(transform_specification[k], widget.value())
                for k, widget in self._transform_sliders.items()
            }
            for k in self._canvas_vars:
                if k.endswith('_x'):
                    transform_context[k] = self._pointer_x
                    if transform_context[k] is None:
                        transform_context = None
                        break
                if k.endswith('_y'):
                    transform_context[k] = self._pointer_y
                    if transform_context[k] is None:
                        transform_context = None
                        break

        # request update service
        try:
            self._interference_update_proxy.wait_for_service(timeout=1)
            self._interference_update_proxy(transform_type, json.dumps(transform_context))
        except rospy.service.ServiceException:
            pass
        except rospy.ROSException:
            pass


class SubscriberThread(QtCore.QThread):
    UpdateEvent = namedtuple('UpdateEvent', ['updates'])
    on_update = QtCore.pyqtSignal(object)

    def __init__(self, sub_listeners, refresh_rate=30):
        super().__init__()
        self._sub_listeners = sub_listeners
        self._refresh_rate = refresh_rate

        # init updates
        self._sub_updates = {key: None for key, manager in self._sub_listeners.items()}

        # init subscribers
        self._subs = {
            key: rospy.Subscriber(manager.topic, manager.data_class, callback=manager.callback)
            for key, manager in self._sub_listeners.items()
        }

    def run(self):
        try:
            rate = rospy.Rate(self._refresh_rate)
            while self.isRunning() and not rospy.is_shutdown():
                # get most current messages from subscriptions and update with newest messages
                for key, manager in self._sub_listeners.items():
                    msg = manager.fetch_newest_message()
                    if key in self._sub_updates and msg is not None:
                        self._sub_updates[key] = msg

                # fire update event
                try:
                    self.on_update.emit(self.UpdateEvent(deepcopy(self._sub_updates)))
                except AttributeError:
                    pass

                # sleep until next iteration
                rate.sleep()
        except rospy.ROSInterruptException:
            pass

        # unregister subscribers
        for topic, sub in self._subs.items():
            sub.unregister()


def clickable(widget):
    class Filter(QtCore.QObject):
        clicked = QtCore.pyqtSignal(object)
        def eventFilter(self, obj, event):
            if obj == widget:
                if event.type() == QtCore.QEvent.MouseButtonRelease:
                    if obj.rect().contains(event.pos()):
                        self.clicked.emit(event)
                        return True
            return False
    fltr = Filter(widget)
    widget.installEventFilter(fltr)
    return fltr.clicked


def transform_slider_caption(v_range, idx):
    # determine slider caption from slider index
    if len(v_range) == 2:
        return '{:.2f}'.format((idx / 100) * (v_range[-1] - v_range[0]) + v_range[0])
    else:
        return '{}'.format(v_range[int((idx / 100) * (len(v_range) - 1))])


def transform_slider_index(v_range, v):
    # determine slider index from transform value
    if len(v_range) == 2:
        return (v - v_range[0]) / (v_range[-1] - v_range[0]) * 100
    else:
        return (v_range.index(v) / len(v_range)) * 100


def transform_slider_value(v_range, idx):
    # determine slider value from slider index
    if len(v_range) == 2:
        return (idx / 100) * (v_range[-1] - v_range[0]) + v_range[0]
    else:
        return v_range[int((idx / 100) * (len(v_range) - 1))]

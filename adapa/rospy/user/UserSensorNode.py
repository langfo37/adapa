import rospy


class UserSensorNode(object):
    def __init__(
            self,
            name='user_sensor',
            topic=None,
            data_class=None,
            refresh_rate=15,
    ):
        self._topic = topic
        self._data_class = data_class
        self._refresh_rate = refresh_rate

        # initialize node
        rospy.init_node(name)
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # initialize publisher
        self._pub = None
        if self._topic is not None and self._data_class is not None:
            self._pub = rospy.Publisher(self._topic, self._data_class, queue_size=10)

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))
        rate = rospy.Rate(self._refresh_rate)
        while not rospy.is_shutdown():
            # create and publish message
            msg = self._create_message()
            if isinstance(msg, self._data_class):
                self._pub.publish(msg)

            # sleep until next iteration
            rate.sleep()

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        # unregister publisher
        if self._pub is not None:
            self._pub.unregister()

    def _create_message(self):
        raise NotImplementedError()

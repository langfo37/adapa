from adapa_ros.msg import JetbotCameraMessage
from adapa_ros.srv import AdapaInterferenceUpdateService
from anunnaki.core.transformation.image import load_transform
from queue import Queue
from PIL import Image
import json
import io
import roslib
import rospy


class AdapaInterferenceNode(object):
    def __init__(
            self,
            name='adapa_interference',
            data_class=rospy.msg.AnyMsg,
            sub_topic='/adapa/interference/source',
            srv_topic='/adapa/interference/update',
            pub_topic='/adapa/interference/target',
            refresh_rate=15,
    ):
        self._data_class = data_class
        self._pub_topic = pub_topic
        self._sub_topic = sub_topic
        self._srv_topic = srv_topic
        self._refresh_rate = refresh_rate
        self._transform = None

        # initialize node
        rospy.init_node(name)
        rospy.loginfo('Initializing {}.'.format(self.__class__.__name__))

        # initialize publisher
        self._pub = None
        if self._pub_topic is not None and self._data_class is not None:
            self._pub = rospy.Publisher(self._pub_topic, self._data_class, queue_size=10)

        # initialize subscriber
        self._sub_msg_queue = Queue()
        self._sub = None
        if self._sub_topic is not None and self._data_class is not None:
            self._sub = rospy.Subscriber(self._sub_topic, self._data_class, callback=self._message_cb)

        # init services
        rospy.Service(self._srv_topic, AdapaInterferenceUpdateService, self._transform_update)
        self._request_queue = Queue()

    def run(self):
        rospy.loginfo('Running {}.'.format(self.__class__.__name__))
        rate = rospy.Rate(self._refresh_rate)
        while not rospy.is_shutdown():
            # get newest message
            sub_msg = None
            while self._sub_msg_queue.qsize() > 0:
                sub_msg = self._sub_msg_queue.get(block=False)

            # process message
            if isinstance(sub_msg, rospy.msg.AnyMsg):
                # dynamically resubscribe to given message type
                sub_topic_type = sub_msg._connection_header['type']
                self._data_class = roslib.message.get_message_class(sub_topic_type)
                self._sub.unregister()
                self._sub = rospy.Subscriber(self._sub_topic, self._data_class, callback=self._message_cb)
                rospy.loginfo('Updating subscription to "{}" ({}).'.format(self._sub_topic, self._data_class.__name__))
            elif isinstance(sub_msg, self._data_class):
                # transform and publish the message
                pub_msg = self._transform_message(sub_msg)
                if isinstance(pub_msg, self._data_class):
                    self._pub.publish(pub_msg)

            # wait until next iteration
            rate.sleep()

    def terminate(self):
        rospy.loginfo('Terminating {}.'.format(self.__class__.__name__))

        # unregister publisher
        if self._pub is not None:
            self._pub.unregister()

        # unregister subscriber
        if self._sub is not None:
            self._sub.unregister()

    def _transform_message(self, sub_msg):
        # get image from message data
        img = Image.open(io.BytesIO(sub_msg.image.data))

        # transform image
        if self._transform is not None and callable(self._transform):
            img = self._transform(img)

        # convert image into byte stream
        img_bytes = io.BytesIO()
        img.save(img_bytes, format='JPEG')
        img_bytes.flush()

        # create compressed image message
        msg = JetbotCameraMessage()
        msg.header.stamp = rospy.Time.now()
        msg.image.format = 'jpeg'
        msg.image.data = img_bytes.getvalue()
        return msg

    def _transform_update(self, req_msg):
        transform_type = req_msg.transform_type
        transform_context = json.loads(req_msg.transform_context)
        transform_factory = load_transform(transform_type)
        if transform_factory is not None:
            if transform_type == 'fog':
                transform_context['fog_scaling'] = 0.25
            if transform_type == 'lensflare':
                transform_context['lensflare_scaling'] = 0.05
            if transform_type == 'raindrop':
                transform_context['raindrop_scaling'] = 0.1
            self._transform = transform_factory(**transform_context)

    def _message_cb(self, gazebo_msg):
        if isinstance(gazebo_msg, self._data_class):
            self._sub_msg_queue.put(gazebo_msg)

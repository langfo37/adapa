#!/usr/bin/env python3

from adapa.backend.jetbot import CameraServoController
from adapa.rospy.jetbot import JetbotActuatorNode
from adapa_ros.msg import JetbotServosMessage
import rospy


class JetbotServosNode(JetbotActuatorNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._servos = CameraServoController()

    def terminate(self):
        super().terminate()
        if self._servos is None:
            return
        self._servos.stop()

    def _process_message(self, msg):
        if self._servos is None:
            return

        # move camera servos up/down
        if msg.cam_rx < 0:
            self._servos.cam_down()
        elif msg.cam_rx > 0:
            self._servos.cam_up()

        # move camera servos left/right
        if msg.cam_rz < 0:
            self._servos.cam_left()
        elif msg.cam_rz > 0:
            self._servos.cam_right()

        # reset camera servos
        if msg.cam_reset:
            self._servos.cam_reset()


def main():
    # execute the node
    node = JetbotServosNode(
        name='jetbot_servos',
        topic='/jetbot/actuator/servos',
        data_class=JetbotServosMessage,
        refresh_rate=15,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

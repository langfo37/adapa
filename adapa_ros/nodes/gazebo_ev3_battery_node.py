#!/usr/bin/env python3

from adapa.rospy.gazebo import GazeboSensorNode
from adapa_ros.msg import EV3BatteryMessage
import rospy


class GazeboEV3BatteryNode(GazeboSensorNode):
    def _convert_message(self, gazebo_msg):
        msg = EV3BatteryMessage()
        msg.header.stamp = rospy.Time.now()
        msg.status = 'high'
        msg.current = 0.18
        msg.voltage = 8.00
        return msg


def main():
    # execute the node
    node = GazeboEV3BatteryNode(
        name='ev3_battery',
        topic='/ev3/sensor/battery',
        data_class=EV3BatteryMessage,
        gazebo_topic=None,
        gazebo_data_class=None,
        refresh_rate=2,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

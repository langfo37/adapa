#!/usr/bin/env python3

from adapa.backend.jetbot import MotorController
from adapa.rospy.jetbot import JetbotActuatorNode
from adapa_ros.msg import JetbotDriveMessage
import rospy
import time


class JetbotDriveNode(JetbotActuatorNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._motors = MotorController()

    def terminate(self):
        super().terminate()
        if self._motors is None:
            return
        self._motors.stop()

    def _process_message(self, msg):
        if self._motors is None:
            return

        x_mag = max(-1.0, min(1.0, msg.twist.linear.x))
        z_mag = max(-1.0, min(1.0, msg.twist.angular.z))
        if abs(x_mag) <= 0.1 and abs(z_mag) <= 0.1:
            self._motors.drive_stop()
        elif x_mag <= 0.0:
            # reverse driving
            left_motor = x_mag - z_mag
            right_motor = x_mag + z_mag
            self._motors.set_drive_motors(left_motor, right_motor)
        else:
            # forward driving
            left_motor = x_mag + z_mag
            right_motor = x_mag - z_mag
            if abs(z_mag) <= 0.1:
                # NOTE: THIS ADJUSTMENT IS MADE TO ACCOUNT FOR DIFFERENCE IN LEFT AND RIGHT TRACKS
                left_motor -= 0.2
                right_motor += 0.2
            self._motors.set_drive_motors(left_motor, right_motor)

        time.sleep(0.01)


def main():
    # execute the node
    node = JetbotDriveNode(
        name='jetbot_drive',
        topic='/jetbot/actuator/drive',
        data_class=JetbotDriveMessage,
        refresh_rate=15,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

#!/usr/bin/env python3

from adapa.rospy.ev3 import EV3MQTTSensorNode
from adapa_ros.msg import EV3GyroMessage
import rospy


class EV3MQTTGyroNode(EV3MQTTSensorNode):
    def _create_message(self, mqtt_payload):
        if not isinstance(mqtt_payload.get('angle'), int):
            return None

        msg = EV3GyroMessage()
        msg.header.stamp = rospy.Time.now()
        msg.angle = mqtt_payload.get('angle')
        return msg


def main():
    # execute the node
    node = EV3MQTTGyroNode(
        name='ev3_gyro',
        topic='/ev3/sensor/gyro',
        data_class=EV3GyroMessage,
        mqtt_topic='ev3/gyro_sensor',
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

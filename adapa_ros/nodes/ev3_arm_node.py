#!/usr/bin/env python3

from adapa.rospy.ev3 import EV3MQTTActuatorNode
from adapa_ros.msg import EV3ArmMessage
import rospy


class EV3MQTTArmNode(EV3MQTTActuatorNode):
    def _create_mqtt_payload(self, msg):
        mqtt_payload = dict()
        mqtt_payload['action'] = msg.action
        return mqtt_payload


def main():
    # execute the node
    node = EV3MQTTArmNode(
        name='ev3_arm',
        mqtt_topic='ev3/arm_actuator',
        topic='/ev3/actuator/arm',
        data_class=EV3ArmMessage,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

#!/usr/bin/env python3

from adapa.rospy.ev3 import EV3MQTTSensorNode
from adapa_ros.msg import EV3UltrasonicMessage
import rospy


class EV3MQTTUltrasonicNode(EV3MQTTSensorNode):
    def _create_message(self, mqtt_payload):
        if not isinstance(mqtt_payload.get('range'), float):
            return None
        msg = EV3UltrasonicMessage()
        msg.header.stamp = rospy.Time.now()
        msg.range.min_range = 0.0
        msg.range.max_range = 2.55
        msg.range.range = mqtt_payload.get('range')
        return msg


def main():
    # execute the node
    node = EV3MQTTUltrasonicNode(
        name='ev3_ultrasonic',
        topic='/ev3/sensor/ultrasonic',
        data_class=EV3UltrasonicMessage,
        mqtt_topic='ev3/ultrasonic_sensor',
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

#!/usr/bin/env python3

from adapa.backend.workstation import CameraController
from adapa.rospy.user import UserSensorNode
from adapa_ros.msg import UserCameraMessage
import io
import rospy


class UserCameraNode(UserSensorNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._camera = CameraController((160, 120))

    def terminate(self):
        super().terminate()
        if self._camera is None:
            return
        self._camera.stop()
        self._camera = None

    def _create_message(self):
        if self._camera is None:
            return None

        # save camera image to bytes object
        img_bytes = io.BytesIO()
        self._camera.capture_image.save(img_bytes, format='JPEG')
        img_bytes.flush()

        # create compressed image message
        msg = UserCameraMessage()
        msg.header.stamp = rospy.Time.now()
        msg.image.format = 'jpeg'
        msg.image.data = img_bytes.getvalue()
        return msg


def main():
    # execute the node
    node = UserCameraNode(
        name='user_camera',
        topic='/user/sensor/camera',
        data_class=UserCameraMessage,
        refresh_rate=15,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

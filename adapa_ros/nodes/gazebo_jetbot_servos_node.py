#!/usr/bin/env python3

from adapa.rospy.gazebo import GazeboActuatorNode
from adapa_ros.msg import JetbotServosMessage
from std_msgs.msg import Float64MultiArray
import rospy
import threading


class GazeboJetbotServosNode(GazeboActuatorNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._lock = threading.Lock()
        self._cam_rx = 0.0
        self._cam_rz = 0.0

    def _convert_message(self, msg):
        with self._lock:
            if msg.cam_rx < 0:
                self._cam_rx += 0.0872665
            elif msg.cam_rx > 0:
                self._cam_rx -= 0.0872665
            if msg.cam_rz < 0:
                self._cam_rz += 0.0872665
            elif msg.cam_rz > 0:
                self._cam_rz -= 0.0872665
            if msg.cam_reset:
                self._cam_rx = 0.0
                self._cam_rz = 0.0

        gazebo_msg = Float64MultiArray()
        gazebo_msg.data = [self._cam_rx, self._cam_rz]
        return gazebo_msg


def main():
    # execute the node
    node = GazeboJetbotServosNode(
        name='jetbot_servos',
        topic='/jetbot/actuator/servos',
        data_class=JetbotServosMessage,
        gazebo_topic='/gazebo_jetbot_servos/command',
        gazebo_data_class=Float64MultiArray,
        refresh_rate=15,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

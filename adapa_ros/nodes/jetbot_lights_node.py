#!/usr/bin/env python3

from adapa.backend.jetbot import LightController
from adapa.rospy.jetbot import JetbotActuatorNode
from adapa_ros.msg import JetbotLightMessage
import rospy


class JetbotLightsNode(JetbotActuatorNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._lights = LightController()
        self._color = (0, 0, 0)

    def terminate(self):
        super().terminate()
        if self._lights is None:
            return
        self._lights.stop()

    def _process_message(self, msg):
        if self._lights is None:
            return
        try:
            color = msg.color.lstrip('#')
            color = tuple(int(color[i:i + 2], 16) for i in (0, 2, 4))
            if self._color != color:
                self._lights.set_rgb_all(*color)
        except ValueError:
            rospy.logwarn('Failed to set lights to color: "{}"'.format(msg.color))
            pass


def main():
    # execute the node
    node = JetbotLightsNode(
        name='jetbot_lights',
        topic='/jetbot/actuator/lights',
        data_class=JetbotLightMessage,
        refresh_rate=4,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

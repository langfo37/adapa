#!/usr/bin/env python3

from adapa.rospy.control import AdapaControlNode
import rospy


def main():
    # get mode from parameter server
    mode = 'manual'
    if rospy.has_param('adapa_mode'):
        mode = rospy.get_param('adapa_mode')

    # execute the node
    node = AdapaControlNode(mode=mode)
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

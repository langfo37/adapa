#!/usr/bin/env python3

from adapa.rospy.gazebo import GazeboActuatorNode
from adapa_ros.msg import EV3ArmMessage
from std_msgs.msg import Float64
import rospy


class GazeboEV3ArmNode(GazeboActuatorNode):
    def _convert_message(self, msg):
        # relay message to gazebo
        gazebo_msg = Float64()
        gazebo_msg.data = 0.0 if msg.action.lower() == "down" else -1.5708
        return gazebo_msg


def main():
    # execute the node
    node = GazeboEV3ArmNode(
        name='ev3_arm',
        topic='/ev3/actuator/arm',
        data_class=EV3ArmMessage,
        gazebo_topic='/gazebo_ev3_arm/command',
        gazebo_data_class=Float64,
        refresh_rate=15,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

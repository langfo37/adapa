#!/usr/bin/env python3

from adapa.rospy.user import AdapaInterferenceNode
from adapa_ros.msg import JetbotCameraMessage
import rospy


def main():
    # execute the node
    node = AdapaInterferenceNode(
        name='adapa_interference',
        data_class=JetbotCameraMessage,
        sub_topic='/adapa/interference/source',
        pub_topic='/adapa/interference/target',
        refresh_rate=15,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

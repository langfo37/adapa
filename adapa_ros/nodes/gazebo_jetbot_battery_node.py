#!/usr/bin/env python3

from adapa.rospy.gazebo import GazeboSensorNode
from adapa_ros.msg import JetbotBatteryMessage
import rospy


class GazeboJetbotBatteryNode(GazeboSensorNode):
    def _convert_message(self, gazebo_msg):
        msg = JetbotBatteryMessage()
        msg.header.stamp = rospy.Time.now()
        msg.status = "high"
        msg.voltage = 12.0
        return msg


def main():
    # execute the node
    node = GazeboJetbotBatteryNode(
        name='jetbot_battery',
        topic='/jetbot/sensor/battery',
        data_class=JetbotBatteryMessage,
        gazebo_topic=None,
        gazebo_data_class=None,
        refresh_rate=2,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

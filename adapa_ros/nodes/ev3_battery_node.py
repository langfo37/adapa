#!/usr/bin/env python3

from adapa.rospy.ev3 import EV3MQTTSensorNode
from adapa_ros.msg import EV3BatteryMessage
import rospy


class EV3MQTTBatteryNode(EV3MQTTSensorNode):
    def _create_message(self, mqtt_payload):
        if not isinstance(mqtt_payload.get('current'), float):
            return None
        if not isinstance(mqtt_payload.get('voltage'), float):
            return None

        msg = EV3BatteryMessage()
        msg.header.stamp = rospy.Time.now()
        msg.current = mqtt_payload.get('current')
        msg.voltage = mqtt_payload.get('voltage')
        if msg.voltage > 7.5:
            msg.status = 'high'
        elif msg.voltage > 6.5:
            msg.status = 'medium'
        elif msg.voltage > 5:
            msg.status = 'low'
        else:
            msg.status = 'empty'
        return msg


def main():
    # execute the node
    node = EV3MQTTBatteryNode(
        name='ev3_battery',
        topic='/ev3/sensor/battery',
        data_class=EV3BatteryMessage,
        mqtt_topic='ev3/power_sensor',
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

#!/usr/bin/env python3

from adapa.rospy.ev3 import EV3MQTTSensorNode
from adapa_ros.msg import EV3TouchMessage
import rospy


class EV3MQTTTouchNode(EV3MQTTSensorNode):
    def _create_message(self, mqtt_payload):
        # determine if any touch sensor was touched
        touched = mqtt_payload.get('touched')
        if isinstance(touched, list):
            touched = any(touched)
        if not isinstance(touched, bool):
            return None

        msg = EV3TouchMessage()
        msg.header.stamp = rospy.Time.now()
        msg.touched = touched
        return msg


def main():
    # execute the node
    node = EV3MQTTTouchNode(
        name='ev3_touch',
        topic='/ev3/sensor/touch',
        data_class=EV3TouchMessage,
        mqtt_topic='ev3/touch_sensor',
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

#!/usr/bin/env python3

from adapa.rospy.gazebo import GazeboActuatorNode
from adapa_ros.msg import JetbotDriveMessage
from geometry_msgs.msg import Twist
import rospy
import threading


class GazeboJetbotDriveNode(GazeboActuatorNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._lock = threading.Lock()
        self._drive_dx = 0.0
        self._drive_rz = 0.0

    def _convert_message(self, msg):
        with self._lock:
            self._drive_dx = msg.twist.linear.x
            self._drive_rz = msg.twist.angular.z

        gazebo_msg = Twist()
        gazebo_msg.linear.x = self._drive_dx
        gazebo_msg.linear.y = 0.0
        gazebo_msg.linear.z = 0.0
        gazebo_msg.angular.x = 0.0
        gazebo_msg.angular.y = 0.0
        gazebo_msg.angular.z = self._drive_rz
        return gazebo_msg


def main():
    # execute the node
    node = GazeboJetbotDriveNode(
        name='jetbot_drive',
        topic='/jetbot/actuator/drive',
        data_class=JetbotDriveMessage,
        gazebo_topic='/gazebo_jetbot_drive/cmd_vel',
        gazebo_data_class=Twist,
        refresh_rate=15,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

#!/usr/bin/env python3

from adapa.rospy.user import AdapaInterferenceGUINode
import rospy


def main():
    # execute the node
    node = AdapaInterferenceGUINode()
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

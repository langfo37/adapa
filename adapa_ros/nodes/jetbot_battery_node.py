#!/usr/bin/env python3

from adapa.backend.jetbot import BatteryController
from adapa.backend.util import MovingAverageFilter
from adapa.rospy.jetbot import JetbotSensorNode
from adapa_ros.msg import JetbotBatteryMessage
import rospy


class JetbotBatteryNode(JetbotSensorNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._battery = BatteryController()
        self._voltage_filter = MovingAverageFilter(window=10, ignore_outliers=True)

    def terminate(self):
        super().terminate()
        if self._battery is None:
            return
        self._battery.stop()
        self._battery = None

    def _create_message(self):
        if self._battery is None:
            return None

        # get current voltage and determine moving average
        voltage = self._battery.check_voltage()
        voltage = self._voltage_filter(voltage)
        status = self._battery.categorize_status(voltage)

        # create message
        msg = JetbotBatteryMessage()
        msg.header.stamp = rospy.Time.now()
        msg.voltage = voltage
        msg.status = status
        return msg


def main():
    # execute the node
    node = JetbotBatteryNode(
        name='jetbot_battery',
        topic='/jetbot/sensor/battery',
        data_class=JetbotBatteryMessage,
        refresh_rate=2,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

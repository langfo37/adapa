#!/usr/bin/env python3

from adapa.backend.jetbot import BuzzerController
from adapa.rospy.jetbot import JetbotActuatorNode
from adapa_ros.msg import JetbotBuzzerMessage
import rospy


class JetbotBuzzerNode(JetbotActuatorNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._buzzer = BuzzerController()

    def terminate(self):
        super().terminate()
        if self._buzzer is None:
            return
        self._buzzer.buzz_off()
        self._buzzer.stop()

    def _process_message(self, msg):
        if self._buzzer is None:
            return
        if msg.action.lower() == 'on':
            self._buzzer.buzz_on()
        if msg.action.lower() == 'off':
            self._buzzer.buzz_off()


def main():
    # execute the node
    node = JetbotBuzzerNode(
        name='jetbot_buzzer',
        topic='/jetbot/actuator/buzzer',
        data_class=JetbotBuzzerMessage,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

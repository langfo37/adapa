#!/usr/bin/env python3

from adapa.rospy.ev3 import EV3MQTTActuatorNode
from adapa_ros.msg import EV3HandMessage
import rospy


class EV3MQTTHandNode(EV3MQTTActuatorNode):
    def _create_mqtt_payload(self, msg):
        mqtt_payload = dict()
        mqtt_payload['action'] = msg.action
        return mqtt_payload


def main():
    # execute the node
    node = EV3MQTTHandNode(
        name='ev3_hand',
        topic='/ev3/actuator/hand',
        data_class=EV3HandMessage,
        mqtt_topic='ev3/hand_actuator',
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

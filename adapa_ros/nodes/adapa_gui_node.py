#!/usr/bin/env python3

from adapa.rospy.user import AdapaGUINode
import rospy


def main():
    # get capture_dir from parameter server
    if rospy.has_param('adapa_capture_dir'):
        capture_dir = rospy.get_param('adapa_capture_dir')
    else:
        rospy.logwarn('Capture directory path must be set via "adapa_capture_dir" ROS parameter.')
        capture_dir = None

    # execute the node
    node = AdapaGUINode(capture_dir=capture_dir)
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

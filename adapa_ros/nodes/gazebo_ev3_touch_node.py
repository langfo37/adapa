#!/usr/bin/env python3

from adapa.rospy.gazebo import GazeboSensorNode
from adapa_ros.msg import EV3TouchMessage
from gazebo_msgs.msg import ContactsState
import rospy


class GazeboEV3TouchNode(GazeboSensorNode):
    def _convert_message(self, gazebo_msg):
        msg = EV3TouchMessage()
        msg.header.stamp = rospy.Time.now()
        msg.touched = (len(gazebo_msg.states) > 0)
        return msg


def main():
    # execute the node
    node = GazeboEV3TouchNode(
        name='ev3_touch',
        topic='/ev3/sensor/touch',
        data_class=EV3TouchMessage,
        gazebo_topic='/gazebo_ev3_touch_left',
        gazebo_data_class=ContactsState,
        refresh_rate=15,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

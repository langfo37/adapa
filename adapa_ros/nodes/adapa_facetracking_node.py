#!/usr/bin/env python3

from adapa.rospy.detection import AdapaFaceTrackingNode
import rospy


def main():
    # get model_path from parameter server
    if rospy.has_param('adapa_face_landmarks_path'):
        model_path = rospy.get_param('adapa_face_landmarks_path')
    else:
        rospy.logwarn('Face landmarks path must be set via "adapa_face_landmarks_path" ROS parameter.')
        model_path = None

    # execute the node
    node = AdapaFaceTrackingNode(model_path=model_path)
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

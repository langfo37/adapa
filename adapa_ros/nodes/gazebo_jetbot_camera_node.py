#!/usr/bin/env python3

from adapa.rospy.gazebo import GazeboSensorNode
from adapa_ros.msg import JetbotCameraMessage
from sensor_msgs.msg import Image as ImageMessageData
from PIL import Image
import io
import numpy as np
import rospy


class GazeboJetbotCameraNode(GazeboSensorNode):
    def _convert_message(self, gazebo_msg):
        # get uncompressed camera image from gazebo
        img = np.reshape(np.frombuffer(gazebo_msg.data, dtype=np.uint8), newshape=(224, 224, 3))
        img = Image.fromarray(img, mode='RGB')

        # convert image into byte stream
        img_bytes = io.BytesIO()
        img.save(img_bytes, format='JPEG')
        img_bytes.flush()

        # create compressed image message
        msg = JetbotCameraMessage()
        msg.header.stamp = rospy.Time.now()
        msg.image.format = 'jpeg'
        msg.image.data = img_bytes.getvalue()
        return msg


def main():
    # execute the node
    node = GazeboJetbotCameraNode(
        name='jetbot_camera',
        topic='/jetbot/sensor/camera',
        data_class=JetbotCameraMessage,
        gazebo_topic='/gazebo_jetbot_camera/image_raw',
        gazebo_data_class=ImageMessageData,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

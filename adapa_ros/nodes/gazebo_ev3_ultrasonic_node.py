#!/usr/bin/env python3

from adapa.rospy.gazebo import GazeboSensorNode
from adapa_ros.msg import EV3UltrasonicMessage
from sensor_msgs.msg import Range
import rospy


class GazeboEV3UltrasonicNode(GazeboSensorNode):
    def _convert_message(self, gazebo_msg):
        msg = EV3UltrasonicMessage()
        msg.header.stamp = rospy.Time.now()
        msg.range = gazebo_msg
        return msg


def main():
    # execute the node
    node = GazeboEV3UltrasonicNode(
        name='ev3_ultrasonic',
        topic='/ev3/sensor/ultrasonic',
        data_class=EV3UltrasonicMessage,
        gazebo_topic='/gazebo_ev3_ultrasonic',
        gazebo_data_class=Range,
        refresh_rate=15,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

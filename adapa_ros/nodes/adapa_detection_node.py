#!/usr/bin/env python3

from adapa.rospy.detection import AdapaDetectionNode
import rospy


def main():
    # get model_path from parameter server
    if rospy.has_param('adapa_detection_dnn_path'):
        model_path = rospy.get_param('adapa_detection_dnn_path')
    else:
        rospy.logwarn('Object Detector model path must be set via "adapa_detection_dnn_path" ROS parameter.')
        model_path = None

    # execute the node
    node = AdapaDetectionNode(model_path=model_path)
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    # suppress warnings
    import warnings
    warnings.simplefilter(action='ignore', category=FutureWarning)
    warnings.simplefilter(action='ignore', category=UserWarning)

    main()

#!/usr/bin/env python3

from adapa.rospy.ev3 import EV3MQTTActuatorNode
from adapa_ros.msg import EV3SpeechMessage
import rospy


class EV3MQTTSpeechNode(EV3MQTTActuatorNode):
    def _create_mqtt_payload(self, msg):
        mqtt_payload = dict()
        mqtt_payload['text'] = msg.text
        return mqtt_payload


def main():
    # execute the node
    node = EV3MQTTSpeechNode(
        name='ev3_speech',
        topic='/ev3/actuator/speech',
        data_class=EV3SpeechMessage,
        mqtt_topic='ev3/speech_actuator',
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()


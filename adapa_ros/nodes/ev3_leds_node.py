#!/usr/bin/env python3

from adapa.rospy.ev3 import EV3MQTTActuatorNode
from adapa_ros.msg import EV3LEDsMessage
import rospy


class EV3MQTTLEDsNode(EV3MQTTActuatorNode):
    def _create_mqtt_payload(self, msg):
        mqtt_payload = dict()
        mqtt_payload['left_color'] = msg.left_color
        mqtt_payload['right_color'] = msg.right_color
        return mqtt_payload


def main():
    # execute the node
    node = EV3MQTTLEDsNode(
        name='ev3_leds',
        topic='/ev3/actuator/leds',
        data_class=EV3LEDsMessage,
        mqtt_topic='ev3/leds_actuator',
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

#!/usr/bin/env python3

from adapa.rospy.gazebo import GazeboActuatorNode
from adapa_ros.msg import EV3HandMessage
from std_msgs.msg import Float64MultiArray
import rospy


class GazeboEV3HandNode(GazeboActuatorNode):
    def _convert_message(self, msg):
        gazebo_msg = Float64MultiArray()
        gazebo_msg.data = [0.0, 0.0] if msg.action.lower() == "close" else [0.523599, -0.523599]
        return gazebo_msg


def main():
    # execute the node
    node = GazeboEV3HandNode(
        name='ev3_hand',
        topic='/ev3/actuator/hand',
        data_class=EV3HandMessage,
        gazebo_topic='/gazebo_ev3_hand/command',
        gazebo_data_class=Float64MultiArray,
        refresh_rate=15,
    )
    try:
        node.run()
    except rospy.ROSInterruptException:
        pass
    node.terminate()


if __name__ == '__main__':
    main()

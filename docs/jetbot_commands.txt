# enable jetbot app
sudo systemctl enable jetbot_start

# disable jetbot app
sudo systemctl disable jetbot_start

# start jetbot app
sudo systemctl start jetbot_start

# stop jetbot app
sudo systemctl stop jetbot_start

# add read/write permission to ttyTHS1
sudo chmod 666 /dev/ttyTHS1
